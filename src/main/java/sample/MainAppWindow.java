package sample;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import sample.dialog.route.GenerationDialog;
import sample.dialog.route.RouteShowDialog;
import sample.models.*;
import sample.util.Action;
import sample.util.ItemView;
import sample.util.SensorUtils;
import sample.util.TreeNode;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MainAppWindow implements Initializable {
    @FXML
    public AnchorPane nodeContainer;

    @FXML
    public Group areaGroup;

    @FXML
    public Slider zoomSlider;

    /**area controls*/
    @FXML
    public TextField areaMaxX,areaMaxY,areaSensors;

    /**hole controls*/
    @FXML
    public TextField holeMinX,holeMinY,holeMaxX,holeMaxY;

    /**controls*/
    @FXML
    public TextField centralPointId, ringsRadius;
    @FXML
    public TextField centralPointIdRouting, ringsRadiusRouting;
    @FXML
    public TextField areaRouteStartId, areaRouteEndId, areaRouteRadius;

    @FXML
    public TextField generationStartId, generationEndId, generationRadius, generationCenterPoint;

    @FXML
    public TextField waysStartId, waysEndId, waysRadius;

    /**generation fields*/
    @FXML
    public TextField generationFieldMinWidth, generationFieldMaxWidth,
            generationFieldMinHeight, generationFieldMaxHeight,
            generationFieldMinSensors, generationFieldMaxSensors,

            generationHoleMinWidth,generationHoleMaxWidth,
            generationHoleMinHeight,generationHoleMaxHeight,

            generationCount;

    /**rigs*/
    @FXML
    public Label ringsTextCount,waysText;
    @FXML
    public ListView<DeviceRing> ringsListView;
    @FXML
    public ListView<String> waysListView;

    /**generation fields*/
    @FXML
    public TextField experimentCentralPointIdRouting, experimentRingsRadiusRouting,
            experimentRingsCount, experimentStartId,
            experimentEndId;

    public Scene scene;

    private Paint[] ringPaints = new Paint[]{
            new Color(1,0,0,1),
            new Color(0,1,0,1),
            new Color(0,0,1,1),
            new Color(1,1,0,1),
            new Color(1,0,1,1),
            new Color(0,1,1,1),
    };



    public static final double ItemSize = 40;

    public List<Device> devices;
    private List<Sensor> sensors;
    private Rings rings;
    private List<DeviceRing> deviceRings;
    private Map<Device, ItemView> deviceView = new HashMap<>();
    private Random random;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        random = new Random(Calendar.getInstance().getTimeInMillis());
        zoomSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            nodeContainer.setScaleX(newValue.doubleValue());
            nodeContainer.setScaleY(newValue.doubleValue());
        });
        ringsListView.setItems(FXCollections.observableArrayList());
        ringsListView.setCellFactory(param -> new Cell(this::drawRing));
        waysListView.setItems(FXCollections.observableArrayList());
    }

    private List<TreeNode<String>> generateRandomGrid(int depth, int depthMax, int maxChildCount, String address){
        List<TreeNode<String>> result = new ArrayList<>();
        if(depth >= depthMax) return result;
        int childCount =  1 + random.nextInt(maxChildCount);
        for(int i= 0; i< childCount; i++){
            String newArddress = address + i;
            result.add(new TreeNode<>(newArddress, generateRandomGrid(depth +1, depthMax, maxChildCount, newArddress)));
        }
        return result;

    }

    public void createNewArea(){
        try {


            int maxX = Integer.parseInt(areaMaxX.textProperty().get());
            int maxY = Integer.parseInt(areaMaxY.textProperty().get());
            int sensorsCount = Integer.parseInt(areaSensors.textProperty().get());

            createNewAreaInt(maxX, maxY, sensorsCount);

        } catch (Exception ignored){
        }
    }

    public void createNewAreaInt(int maxX, int maxY, int sensorsCount) {
        nodeContainer.getChildren().clear();
        nodeContainer.setPrefSize(ItemSize * maxX, ItemSize * maxY);

        sensors = SensorUtils.generateField(maxX,maxY,sensorsCount);
        devices = SensorUtils.generateDevices(sensors);

        drawArea();
    }

    public void createHole(){
        if(isAreaCreated()){
            try {
                int minX = Integer.parseInt(holeMinX.textProperty().get());
                int minY = Integer.parseInt(holeMinY.textProperty().get());
                int maxX = Integer.parseInt(holeMaxX.textProperty().get());
                int maxY = Integer.parseInt(holeMaxY.textProperty().get());

                createHoleBound(minX, minY, maxX, maxY);
            } catch (Exception ignore){
            }
        }
    }

    public void createHoleBound(int minX, int minY, int maxX, int maxY) {
        SensorUtils.createHoleInArea(devices, minX, minY, maxX, maxY, (Device device)->{
            nodeContainer.getChildren().remove(deviceView.get(device).getContainer());
            deviceView.remove(device);
            sensors.removeAll(device.getSensors());
        });
    }

    public void calculateRings(){
        if(!isAreaCreated()) return;

        try{
            int pointId = Integer.parseInt(centralPointId.textProperty().get());
            double radius = Double.parseDouble(ringsRadius.textProperty().get());

            createRings(pointId, radius);


        } catch (Exception ignore){
        }
    }

    public void createRings(int pointId, double radius) {
        rings = new RingCreator(sensors, devices).createRingsForCentralPoint(pointId, radius);

        deviceRings =
                IntStream.range(0, rings.getSize())
                        .mapToObj((index) -> {
                            List<Integer> listSensorId = rings.getRing(index);
                            return new DeviceRing(devices.stream().filter(device ->
                                    device.getSensors()
                                            .stream().filter(sensor -> listSensorId.contains(sensor.getId()))
                                            .findAny().orElse(null) != null).collect(Collectors.toList()), index);
                        })
                        .collect(Collectors.toList());

        /**clear field*/
        devices.forEach((device -> deviceView.get(device).getCircle().setFill(new Color(0,0,0,0))));

        int colorIndex = 0;
        for (DeviceRing ringDevices :
                deviceRings) {
            ringDevices.setRingPaint(ringPaints[colorIndex]);
            ringDevices.setDraw(true);
            drawRing(ringDevices);

            colorIndex++;
            if(colorIndex >= ringPaints.length){
                colorIndex = 0;
            }
        }

        ringsListView.getItems().clear();
        ringsListView.getItems().addAll(deviceRings);
        ringsTextCount.setText("" + deviceRings.size());
    }

    public void calculateAreaRoute(){
        if(!isAreaCreated()) return;

        try{
            int startPointId = Integer.parseInt(areaRouteStartId.textProperty().get());
            int endPointId = Integer.parseInt(areaRouteEndId.textProperty().get());
            double radius = Double.parseDouble(areaRouteRadius.textProperty().get());

            calculateAreaRoute(startPointId, endPointId, radius);
        } catch (Exception ignore){
        }
    }

    public void drawRoutes(List<Device> area, List<Device> gpsr, List<Device> ghfs){
        double newSize = 20;
        nodeContainer.getChildren().clear();

        deviceView.keySet().stream()
                .filter(device -> !area.contains(device) && !gpsr.contains(device) && !ghfs.contains(device))
                .forEach(device -> {
                    ItemView view = deviceView.get(device);
                    view.changeSize(newSize);
                    view.getText().setText("");
                    view.getCircle().setStroke(new Color(0,0,0,0.5));
                    view.getCircle().setFill(new Color(0,0,0,0));
                    nodeContainer.getChildren().add(view.getContainer());
                });

        IntStream.range(0, area.size() -1).forEach(i->{
            Device first = area.get(i);
            Device second = area.get(i + 1);
            Line line = getLine(first, second);
            line.getStrokeDashArray().addAll(10d,8d);
            line.setStrokeWidth(3);
            line.setStroke(new Color(0,0,0,1));

            nodeContainer.getChildren().add(line);
        });

        IntStream.range(0, ghfs.size() -1).forEach(i->{
            Device first = ghfs.get(i);
            Device second = ghfs.get(i + 1);
            Line line = getLine(first, second);
            line.setStrokeWidth(3);
            line.setStroke(new Color(0,0,0,1));

            nodeContainer.getChildren().add(line);
        });

        IntStream.range(0, gpsr.size() -1).forEach(i->{
            Device first = gpsr.get(i);
            Device second = gpsr.get(i + 1);
            Line line = getLine(first, second);

            line.setStrokeWidth(3);
            line.setStroke(new Color(0,0,0,1));
            nodeContainer.getChildren().add(line);

            double lineVectorX = line.getEndX() - line.getStartX();
            double lineVectorY = line.getEndY() - line.getStartY();

            double length = Math.sqrt(lineVectorX*lineVectorX + lineVectorY*lineVectorY);

            lineVectorX /=length;
            lineVectorY /=length;

            double perpendicularX = -lineVectorY;
            double perpendicularY = lineVectorX;

            double crossSize = 10;

            double firstPointX = line.getStartX() + lineVectorX*(length/2 - crossSize/2);
            double firstPointY = line.getStartY() + lineVectorY*(length/2 - crossSize/2);

            double secondPointX = line.getStartX() + lineVectorX*(length/2 + crossSize/2);
            double secondPointY = line.getStartY() + lineVectorY*(length/2 + crossSize/2);


            Line crossFirstLine = new Line(
                    firstPointX + perpendicularX*(crossSize/2),
                    firstPointY + perpendicularY*(crossSize/2),
                    secondPointX - perpendicularX*(crossSize/2),
                    secondPointY - perpendicularY*(crossSize/2));

            crossFirstLine.setStrokeWidth(3);
            crossFirstLine.setStroke(new Color(0,0,0,1));
            nodeContainer.getChildren().add(crossFirstLine);

            crossFirstLine = new Line(
                    firstPointX - perpendicularX*(crossSize/2),
                    firstPointY - perpendicularY*(crossSize/2),
                    secondPointX + perpendicularX*(crossSize/2),
                    secondPointY + perpendicularY*(crossSize/2));
            crossFirstLine.setStrokeWidth(3);
            crossFirstLine.setStroke(new Color(0,0,0,1));
            nodeContainer.getChildren().add(crossFirstLine);
        });

        area.stream().filter((device -> !gpsr.contains(device) && !ghfs.contains(device)))
                .forEach(device -> {
                    ItemView view = deviceView.get(device);
                    view.getCircle().setFill(new Color(1,1,1,1));
                    view.changeSize(newSize);
                    view.getText().setText("");
                    nodeContainer.getChildren().add(view.getContainer());
                });
        gpsr.stream().filter((device -> !ghfs.contains(device)))
                .forEach(device -> {
            ItemView view = deviceView.get(device);
            view.getCircle().setFill(new Color(1,1,1,1));
            view.changeSize(newSize);
            view.getText().setText("");
            nodeContainer.getChildren().add(view.getContainer());
        });

        ghfs.forEach(device -> {
                    ItemView view = deviceView.get(device);
                    view.getCircle().setFill(new Color(1,1,1,1));
                    view.changeSize(newSize);
                    view.getText().setText("");
                    nodeContainer.getChildren().add(view.getContainer());
                });
    }

    public void drawRoute(List<Device> firstRoute, List<Device> secondRoute){
        nodeContainer.getChildren().clear();

        deviceView.keySet().stream()
                .filter(device -> !firstRoute.contains(device) && !secondRoute.contains(device))
                .forEach(device -> {
                    ItemView view = deviceView.get(device);
                    view.getText().setText("");
                    view.getCircle().setStroke(new Color(0,0,0,0.5));
                    view.getCircle().setFill(new Color(0,0,0,0));
                    nodeContainer.getChildren().add(view.getContainer());
                });

        IntStream.range(0, firstRoute.size() -1).forEach(i->{
            Device first = firstRoute.get(i);
            Device second = firstRoute.get(i + 1);
            Line line = getLine(first, second);
            line.getStrokeDashArray().addAll(25d, 20d, 5d, 20d);
            line.setStrokeWidth(3);
            line.setStroke(new Color(0,0,0,1));
            nodeContainer.getChildren().add(line);
        });

        IntStream.range(0, secondRoute.size() -1).forEach(i->{
            Device first = secondRoute.get(i);
            Device second = secondRoute.get(i + 1);
            Line line = getLine(first, second);
            line.getStrokeDashArray().addAll(5d, 10d);
            line.setStrokeWidth(3);
            line.setStroke(new Color(0,0,0,1));
            nodeContainer.getChildren().add(line);
        });

        firstRoute.stream().filter((device -> !secondRoute.contains(device)))
                .forEach(device -> {
            ItemView view = deviceView.get(device);
            view.getCircle().setFill(new Color(1,1,1,1));
            nodeContainer.getChildren().add(view.getContainer());
        });
        secondRoute.forEach(device -> {
                    ItemView view = deviceView.get(device);
                    view.getCircle().setFill(new Color(1,1,1,1));
                    nodeContainer.getChildren().add(view.getContainer());
                });
    }

    public Line getLine(Device first, Device second) {
        return new Line(first.getX()*ItemSize + ItemSize/2, first.getY()*ItemSize + ItemSize/2,
                second.getX()*ItemSize + ItemSize/2, second.getY()*ItemSize + ItemSize/2);
    }

    public List<List<Device>> calculateAreaRoute(int startPointId, int endPointId, double radius) {
        List<List<Device>> list = new AreaRouteCreator(sensors, devices).calculateRoutes(startPointId,endPointId, radius);

        /**clear field*/
        devices.forEach((device -> deviceView.get(device).getCircle().setFill(new Color(0,0,0,0))));

        int colorIndex = 0;
        for (List<Device> ringDevices :
                list) {
            Paint color = ringPaints[colorIndex];

            for(Device device: ringDevices){
                deviceView.get(device).getCircle().setFill(color);
            }

            colorIndex++;
            if(colorIndex >= ringPaints.length){
                colorIndex = 0;
            }
        }

        return list;
    }

    public void calculateWays(){
        if(!isAreaCreated()) return;

        try{
            int startPointId = Integer.parseInt(waysStartId.textProperty().get());
            int endPointId = Integer.parseInt(waysEndId.textProperty().get());
            double radius = Double.parseDouble(waysRadius.textProperty().get());
            waysListView.getItems().clear();

            List<List<Device>> result = calculateWaysInternal(startPointId, endPointId, radius);   new AreaRouteCreator(sensors, devices).findWays(startPointId, endPointId, radius);
            //List<Device> devices1 = GF2.findRouteByGPSR(startPointId, endPointId, radius, devices);

            //result.add(devices1);

            result.forEach(el-> waysListView.getItems().add(getWayString(el)));



            waysText.setText("Ways: " + result.size());
        } catch (Exception ignore){
        }
    }

    public String getWayString(List<Device> el){
        return String.join(" -> ", el.stream().map(el1->getDeviceId(el1).split(",")[0]).collect(Collectors.toList()));
    }

    private List<List<Device>> calculateWaysInternal(int startIndex, int endIndex, double radius){
        Device startDevice = Device.getDeviceBySensorId(devices, startIndex);

        List<List<Device>> result = new AreaRouteCreator(sensors, devices).findWays(startIndex, endIndex, radius);
        result.forEach((el->el.add(0,startDevice)));
        return result;
    }

    public void toggleAllRings(){
        if(deviceRings != null){
            boolean isDrawAll = !deviceRings.stream()
                    .map(DeviceRing::isDraw)
                    .reduce(true, (left, right)-> left && right);

            deviceRings.forEach((deviceRing -> {
                deviceRing.setDraw(isDrawAll);
                drawRing(deviceRing);
            }));

            forceListRefreshOn(ringsListView);
        }
    }
    public void calculateRoutingTable(){
        if(!isAreaCreated()) return;

        try {
            int pointId = Integer.parseInt(centralPointIdRouting.textProperty().get());
            double radius = Double.parseDouble(ringsRadiusRouting.textProperty().get());

            RouteShowDialog dialog = calculateRoutingTable(pointId, radius);

            saveWritableToFile(dialog.mainContainer.snapshot(new SnapshotParameters(), null), "D:\\Projects\\JavaProjects\\WSSExtention\\images.png");
        } catch (Exception ignore){
        }
    }

    public RouteShowDialog calculateRoutingTable(int pointId, double radius) {
        return calculateRoutingTable(pointId, radius, Integer.MAX_VALUE);
    }

    public RouteShowDialog calculateRoutingTable(int pointId, double radius, int maxCount) {
        return calculateRoutingTable(pointId, radius, maxCount, null);
    }

    public RouteShowDialog calculateRoutingTable(int pointId, double radius, int maxCount, List<Device> mainDevices) {
        Device device = devices.stream()
                .filter(deviceInner -> deviceInner.getSensors().stream().filter(sensor -> sensor.getId() == pointId).findFirst().orElse(null) != null)
                .findFirst().orElse(null);


        if(device !=null){
            devices.forEach(deviceInner-> deviceInner.setAddress(null));
            device.setAddress("");

            new RouteCreator(devices).calculateRoutingTable(device, radius);

            RouteShowDialog dialog = new RouteShowDialog(devices, maxCount, mainDevices);
            dialog.show();
            return  dialog;
        }
        return null;
    }

    public void startGeneration(){
        try{
            int areaMinWidth = Integer.parseInt(generationFieldMinWidth.textProperty().get());
            int areaMaxWidth = Integer.parseInt(generationFieldMaxWidth.textProperty().get());
            int areaMinHeight = Integer.parseInt(generationFieldMinHeight.textProperty().get());
            int areaMaxHeight = Integer.parseInt(generationFieldMaxHeight.textProperty().get());

            int areaMinSensors = Integer.parseInt(generationFieldMinSensors.textProperty().get());
            int areaMaxSensors = Integer.parseInt(generationFieldMaxSensors.textProperty().get());

            int holeMinWidth = Integer.parseInt(generationHoleMinWidth.textProperty().get());
            int holeMaxWidth = Integer.parseInt(generationHoleMaxWidth.textProperty().get());
            int holeMinHeight = Integer.parseInt(generationHoleMinHeight.textProperty().get());
            int holeMaxHeight = Integer.parseInt(generationHoleMaxHeight.textProperty().get());

            int generationCo = Integer.parseInt(generationCount.textProperty().get());

            new GenerationDialog(areaMinWidth, areaMaxWidth, areaMinHeight, areaMaxHeight,areaMinSensors, areaMaxSensors,holeMinWidth, holeMaxWidth, holeMinHeight, holeMaxHeight , generationCo, this).show();

            /*for (int i = 0; i < generationCo; i++) {
                createNewAreaInt(areaMinWidth + random.nextInt(areaMaxWidth - areaMinWidth),
                        areaMinHeight + random.nextInt(areaMaxHeight - areaMinHeight),
                        areaMinSensors + random.nextInt(areaMaxSensors - areaMinSensors));
                WritableImage snapshot = areaGroup.snapshot(new SnapshotParameters(), null);


                File file = new File("test.png");
                RenderedImage renderedImage = SwingFXUtils.fromFXImage(snapshot, null);
                ImageIO.write(
                        renderedImage,
                        "png",
                        file);
            }*/


        } catch (Exception ignore){
            boolean a = false;
        }
    }

    public void experementGeneration(){
        try{
            int startPointId = Integer.parseInt(experimentStartId.textProperty().get());
            int endPointId = Integer.parseInt(experimentEndId.textProperty().get());
            int centerPoint = Integer.parseInt(experimentCentralPointIdRouting.textProperty().get());
            double radius = Double.parseDouble(experimentRingsRadiusRouting.textProperty().get());
            int ringsCount = Integer.parseInt(experimentRingsCount.textProperty().get());
            int upscale = 3;
            WritableImage image = createScaledView(areaGroup, upscale);//areaGroup.snapshot(new SnapshotParameters(), null);
            saveWritableToFile(image, "D:\\experiment\\configuration.png");

            createRings(centerPoint, radius);
            List<List<Integer>> startRings = new ArrayList<>();

            for (int i = 0; i < ringsCount; i++) {
                startRings.add(rings.getRing(i+1));
            }

            int maxCount = 4;

            exportData();
            RouteShowDialog dialog = calculateRoutingTable(centerPoint,radius, maxCount);
            image = createScaledView(dialog.mainContainer, upscale);//.snapshot(new SnapshotParameters(), null);
            saveWritableToFile(image, "D:\\experiment\\main_central_point_routing_table.png");
            dialog.close();

            //List<Device> wayGFHS = GetGFHSRoute(startPointId, endPointId, radius);
            List<Device> wayGFHS = GetGFHSRouteByAddresses(startPointId, endPointId, radius);
            String way = getWayString(wayGFHS);

            List<String> firstCol = new ArrayList<>();
            List<String> secondCol = new ArrayList<>();
            firstCol.add("Title");
            secondCol.add("Value");

            firstCol.add("Hop Count");
            secondCol.add("" + wayGFHS.size());

            firstCol.add("Way sample");
            secondCol.add("" + way);

            List<Device> mainDevices = devices.stream()
                    .filter(device -> device.getAddress() != null)
                    .filter(device -> RouteShowDialog.charCount(device.getAddress(), '0')< maxCount)
                    .collect(Collectors.toList());

            int saveOriginal = wayGFHS.size();

            for (int i = 0; i < startRings.size(); i++) {
                for (Integer id: startRings.get(i)) {
                    createRings(id, radius);
                    //if(id != 105 && id != 53) continue;
                    try{
                        dialog = calculateRoutingTable(id,radius, maxCount, mainDevices);
                        image = createScaledView(dialog.mainContainer, upscale);//.snapshot(new SnapshotParameters(), null);
                        saveWritableToFile(image, String.format("D:\\experiment\\next_hop_routing_table_%05d_%d.png", i+1,id));
                        dialog.close();
                    } catch (Exception ex){
                        continue;
                    }


                    wayGFHS = GetGFHSRoute(startPointId, endPointId, radius);

                    if(wayGFHS == null) continue;

                    firstCol.add("Central point Id");
                    secondCol.add("" + id);

                    firstCol.add("Nex hop number");
                    secondCol.add("" + (i+1));

                    firstCol.add("Hop Count");
                    secondCol.add("" + wayGFHS.size());

                    firstCol.add("Way sample");
                    secondCol.add("" + getWayString(wayGFHS));

                    firstCol.add("Diff Count");
                    secondCol.add("" + (saveOriginal -wayGFHS.size()));
                }
            }

            List<String> result = new ArrayList<>();
            for (int i = 0; i < firstCol.size(); i++) {
                result.add(firstCol.get(i)+","+secondCol.get(i));
            }

            FileWriter writer = new FileWriter("D:\\experiment\\result.txt");
            for(String str: result) {
                writer.write(str + "\r\n");
            }
            writer.close();



        } catch (Exception ignore){
            boolean a = false;
        }
    }

    private static WritableImage createScaledView(Node node, int scale) {
        final Bounds bounds = node.getLayoutBounds();

        final WritableImage image = new WritableImage(
                (int) Math.round(bounds.getWidth() * scale),
                (int) Math.round(bounds.getHeight() * scale));

        final SnapshotParameters spa = new SnapshotParameters();
        spa.setTransform(javafx.scene.transform.Transform.scale(scale, scale));

        return node.snapshot(spa, image);
    }

    private List<List<String>> _innerExport;
    private int index12 = 0;
    public void initTestGeneration(){
        _innerExport = new ArrayList<>();
        index12 = 0;
    }
    public void nextTestGeneration(){
        if(!isAreaCreated()) return;

        try{
            int startPointId = Integer.parseInt(generationStartId.textProperty().get());
            int endPointId = Integer.parseInt(generationEndId.textProperty().get());
            int centerPoint = Integer.parseInt(generationCenterPoint.textProperty().get());
            double radius = Double.parseDouble(generationRadius.textProperty().get());

            List<String> result = new ArrayList<>();
            _innerExport.add(result);

            List<Device> wayNM = calculateWaysInternal(startPointId, endPointId, radius).get(0);
            List<Device> wayGPSR = GF2.findRouteByGPSR(startPointId, endPointId, radius, devices);//calculateWaysInternal(startPointId, endPointId, radius).get(0);
            createRings(centerPoint, radius);
            RouteShowDialog dialog = calculateRoutingTable(centerPoint,radius);
            WritableImage image = dialog.mainContainer.snapshot(new SnapshotParameters(), null);
            saveWritableToFile(image, index12 + "_routing_table.png");
            dialog.close();
            List<Device> wayGFHS = GetGFHSRoute(startPointId, endPointId, radius);

            drawArea();

            image = areaGroup.snapshot(new SnapshotParameters(), null);
            saveWritableToFile(image, index12 + "_original_image.png");

            drawRoutes(wayNM,wayGPSR, wayGFHS);
            image = areaGroup.snapshot(new SnapshotParameters(), null);
            saveWritableToFile(image, index12+"_routes_combination.png");

            result.add("" + wayNM.size());
            result.add("" + wayGPSR.size());
            result.add("" + wayGFHS.size());

            result.add(getWayString(wayNM));
            result.add(getWayString(wayGPSR));
            result.add(getWayString(wayGFHS));

            result.add("" + startPointId);
            result.add("" + endPointId);
            result.add("" + radius);
            result.add("" + centerPoint);
        } catch (Exception ignore){
        }
        index12++;
    }

    public int sameHopCountOfAddresses(String firstAddress, String secondAddress){
        int result = 0;
        char stopChar = '0';

        int stepsCount = Math.min(firstAddress.length(), secondAddress.length());

        for (int i =0; i< stepsCount; i++){
            char fChar = firstAddress.charAt(i);
            char sChar = secondAddress.charAt(i);
            if(fChar == stopChar || sChar == stopChar){
                if(fChar == stopChar && sChar == stopChar){
                    result++;
                } else{
                    //different address part
                    break;
                }
            }
        }

        return result;
    }

    public List<Device> GetGFHSRouteByAddresses(int startPointId, int endPointId, double radius){
        try {
            Device startDevice = Device.getDeviceBySensorId(devices, startPointId);
            Device endDevice = Device.getDeviceBySensorId(devices, endPointId);

            String startAddress = startDevice.getAddress();
            String endAddress = endDevice.getAddress();

            List<Device> route = new ArrayList<>();

            Device currentDevice = startDevice;


            //can change to comparing addresses
            while (currentDevice != endDevice){
                route.add(currentDevice);

                Device nextDevice = null;
                int nextDeviceMetric = Integer.MIN_VALUE;

                String fromAddress = currentDevice.getAddress();

                List<Device> neighborhoods =  Device.getDeviceNeighborhoods(devices, currentDevice, radius)
                        .filter(d-> d.getAddress()!= null).collect(Collectors.toList());

                for (Device d: neighborhoods) {
                    String nAddress = d.getAddress();

                    Integer newDeviceMetric = null;
                    //bigger mean closer to destination address

                    //check is neighborhood address is down on tree (from center to destination)
                    if(endAddress.startsWith(nAddress)){
                        // contains number of hop to destination address
                        newDeviceMetric = RouteShowDialog.charCount(nAddress, '0');
                    }
                    //check in neighborhood address is up on tree (from start to center)
                    else if(fromAddress.startsWith(nAddress)){
                        // contains number of hop to central address
                        newDeviceMetric = -RouteShowDialog.charCount(nAddress, '0');
                    }

                    if(newDeviceMetric != null){
                        // if new metric bigger means that device closer to destination address
                        if(nextDeviceMetric < newDeviceMetric){
                            nextDeviceMetric = newDeviceMetric;
                            nextDevice = d;
                        }
                    }
                }

                if(nextDevice == null){
                    //Some error in calculation
                    throw new Exception(String.format("Can't find way from %s to %s", startAddress, endAddress));
                }
                currentDevice = nextDevice;
            }

            route.add(endDevice);
            return route;

        } catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    public List<Device> GetGFHSRoute(int startPointId, int endPointId, double radius){
        try {
            Device fromDevice = Device.getDeviceBySensorId(devices, startPointId);

            Device toDevice = Device.getDeviceBySensorId(devices, endPointId);

            List<Device> newRouteFull = new ArrayList<>();
            List<Device> newRoute = new ArrayList<>();

            newRouteFull.addAll(GenerationDialog.getRouteTree(devices, fromDevice));
            newRouteFull.addAll(GenerationDialog.getRouteTree(devices, toDevice)
                    .stream()
                    .filter(device -> device.getAddress().length() > 0) //filter root device(already included in first)
                    .sorted(Comparator.comparingInt(lhs -> lhs.getAddress().length())) //order by desc from root node
                    .collect(Collectors.toList()));

            int index = 0;
            while (index < newRouteFull.size()) {
                Device device = newRouteFull.get(index);
                newRoute.add(device);
                int curIndex = index;
                Device fromRoute = Device.getDeviceNeighborhoods(devices, device, radius).filter((oneDevice) -> newRouteFull.lastIndexOf(oneDevice) > curIndex).findAny().orElse(null);
                if (fromRoute == null) {
                    index++;
                } else {
                    index = newRouteFull.lastIndexOf(fromRoute);
                }
            }

            return newRoute;
        } catch(Exception ex){
            return null;
        }
    }

    public void finishGeneration(){
        exportData();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select save file");
        fileChooser.getExtensionFilters().add( new FileChooser.ExtensionFilter("Sensor Export File", "csv"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
        fileChooser.setInitialFileName("sensor_export_" + sdf.format(Calendar.getInstance().getTime()) + ".csv");
        File file = fileChooser.showSaveDialog(nodeContainer.getScene().getWindow());

        String[] results = new String[]{
                "","NM", "GPSR", "GFHS","NM Way","GPSR Way","GFHS Way","Start Id","End Id","Radius","Center Point Id"
        };


        for (int i = 0; i < _innerExport.size(); i++) {
            List<String> items = _innerExport.get(i);
            results[0] += "," + (i+1);
            for (int i1 = 0; i1 < items.size(); i1++) {
                String item = items.get(i1);
                results[i1+1] += "," + item;
            }

        }

        if(file!=null) {
            FileWriter fileWriter = null;
            try{
                fileWriter = new FileWriter(file);
                fileWriter.write(String.join("\r\n", results));
            } catch (Exception ignored){
            }
            finally {
                if(fileWriter != null){
                    try {
                        fileWriter.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    private void drawRing(DeviceRing ring){
        for (Device device :
                ring.getDevices()) {
            deviceView.get(device).getCircle().setFill(ring.getRingPaint());
        }
    }

    public void exportData(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select save file");
        fileChooser.getExtensionFilters().add( new FileChooser.ExtensionFilter("Sensor Configuration File", "cfg"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
        fileChooser.setInitialFileName("sensor_config_" + sdf.format(Calendar.getInstance().getTime()) + ".cfg");
        File file = fileChooser.showSaveDialog(nodeContainer.getScene().getWindow());
        if(file!=null) {
            FileWriter fileWriter = null;
            try{
                fileWriter = new FileWriter(file);
                fileWriter.write(new Gson().toJson(sensors));
            } catch (Exception ignored){
            }
             finally {
                if(fileWriter != null){
                    try {
                        fileWriter.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }
    public void importData(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select configuration file");
        fileChooser.getExtensionFilters().add( new FileChooser.ExtensionFilter("Sensor Configuration File", "*.cfg"));

        File file = fileChooser.showOpenDialog(nodeContainer.getScene().getWindow());
        if(file!=null) {
            FileReader reader = null;
            try{
                reader = new FileReader(file);
                Type collectionType = new TypeToken<List<Sensor>>(){}.getType();

                sensors = new Gson().fromJson(reader, collectionType);
                devices = SensorUtils.generateDevices(sensors);
                drawArea();
            } catch (Exception ignored){
            }
            finally {
                if(reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    private boolean isAreaCreated() {
        return devices !=null && !devices.isEmpty();
    }

    private void drawArea() {
        nodeContainer.getChildren().clear();
        deviceView.clear();
        for (Device device : devices
             ) {

            ItemView item = createViewPoint(getDeviceId(device));

            item.getContainer().setLayoutX(device.getX()*ItemSize);
            item.getContainer().setLayoutY(device.getY()* ItemSize);

            nodeContainer.getChildren().add(item.getContainer());
            deviceView.put(device, item);
        }
    }

    private String getDeviceId(Device device) {
        String id = "" + device.getSensors().get(0).getId();
        return device.getSensors().size() > 1 ? id + ",..": id;
    }

    private ItemView createViewPoint(String id){
        return createViewPoint(id,ItemSize);
    }

    private ItemView createViewPoint(String id, double size){
        return new ItemView(id, size);
    }

    private <T> void forceListRefreshOn(ListView<T> lsv) {
        ObservableList<T> items = lsv.<T>getItems();
        lsv.<T>setItems(null);
        lsv.<T>setItems(items);
    }

    private void saveWritableToFile(WritableImage snapshot, String fileName) {
        File file = new File(fileName);
        RenderedImage renderedImage = SwingFXUtils.fromFXImage(snapshot, null);
        try {
            ImageIO.write(
                    renderedImage,
                    "png",
                    file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class Cell extends ListCell<DeviceRing>{
        private Action<DeviceRing> drawRingCallback;

        @FXML
        protected HBox container;
        @FXML
        protected Rectangle ringColor;
        @FXML
        protected Label ringText;

        public Cell(Action<DeviceRing> drawRingCallback) {
            this.drawRingCallback = drawRingCallback;
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/listItem.fxml"));
            fxmlLoader.setController(this);
            try {
                container = fxmlLoader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            this.setPrefWidth(0);
            container.prefWidthProperty().bind(this.widthProperty());
        }

        @Override
        protected void updateItem(DeviceRing item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                clearContent();
            } else {
                addContent(item);
            }
        }

        public void toggleRingView(){
            DeviceRing ring = getItem();
            ring.setDraw(!ring.isDraw());
            ringColor.setFill(ring.getRingPaint());

            drawRingCallback.action(ring);
        }

        private void clearContent() {
            setText(null);
            setGraphic(null);
        }

        private void addContent(DeviceRing cache) {
            setText(null);
            ringColor.setFill(cache.getRingPaint());
            ringText.setText(String.format("Ring %d", cache.getRingNumber()));
            setGraphic(container);
        }
    }
}
