package sample.dialog.route;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurve;
import sample.dialog.route.view.DeviceTreeView;
import sample.draw.clear.ReingoldTilford;
import sample.draw.clear.Tree;
import sample.models.Device;
import sample.models.Sensor;
import sample.util.AbsDialog;
import sample.util.TreeNode;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by dimka on 3/19/2017.
 */
public class RouteShowDialog extends AbsDialog implements Initializable{
    @FXML
    public AnchorPane mainContainer, deviceContainer, lineContainer;

    private TreeNode<Device> mainDevice;

    public static final double ItemSize = 40;
    public static final double ItemPadding = 2;
    public static final double HorizontalSpacing = 280;

    public static int charCount(String data, char charFind){
        int result = 0;
        for (char inChar:data.toCharArray() ) {
            if(inChar == charFind){
                result++;
            }
        }
        return result;
    }
    public RouteShowDialog(List<Device> devices){
        this(devices, Integer.MAX_VALUE);
    }

    public RouteShowDialog(List<Device> devices, int maxCount){
        this(devices, maxCount, null);
    }

    public RouteShowDialog(List<Device> devices, int maxCount, List<Device> additionalDevices){
        super();

        Map<String, Tree<Device>> map = new HashMap<>();

        //List<Device> unMapped = devices.stream().filter(device -> device.getAddress() == null).collect(Collectors.toList());

        devices.stream()
                .filter(device -> device.getAddress() != null)
                .filter(device -> charCount(device.getAddress(), '0')< maxCount)
                .filter(device -> additionalDevices == null || additionalDevices.contains(device))
                .forEach(device -> {
                    String deviceAddress = device.getAddress();
                    String parentAddress = getParentAddress(deviceAddress);
                    Tree<Device> newNode;
                    if (map.containsKey(deviceAddress)) {
                        newNode = new Tree<>(device,ItemSize, ItemSize,0, map.get(deviceAddress).getChild());
                    } else {
                        newNode = new Tree<>(device,ItemSize, ItemSize,0, new ArrayList<>());
                    }
                    map.put(deviceAddress, newNode);

                    if (map.containsKey(parentAddress)) {
                        map.get(parentAddress).getChild().add(newNode);
                    } else if(parentAddress!=null) {
                        ArrayList<Tree<Device>> list = new ArrayList<>();
                        list.add(newNode);
                        map.put(parentAddress, new Tree<>(null,ItemSize, ItemSize, 0, list));
                    }
                });
        Tree<Device> device = map.get("");
        finalPreporation(device, 0);

        try{
            new ReingoldTilford().layout(device);
            double size[] = treeSize(device);
            mainContainer.setMinSize(size[1] + 2*HorizontalSpacing, size[0] + ItemSize);
            mainContainer.setMaxSize(size[1] + 2*HorizontalSpacing, size[0] + ItemSize);
        } catch (Exception ex){
            ex.printStackTrace();
        }


        deviceContainer.getChildren().clear();
        lineContainer.getChildren().clear();

        drawTree(device, null, null);
    }

    private void finalPreporation(Tree<Device> deviceTreeNode, int depth){
        deviceTreeNode.setY(depth * HorizontalSpacing);
        if(deviceTreeNode.getChild().size() == 0) return;

        deviceTreeNode.getChild().sort(Comparator.comparingInt(o -> o.getNode().getAddress().length()));
        deviceTreeNode.getChild().forEach(deviceTree -> finalPreporation(deviceTree, depth +1));
    }

    private <T> double[] treeSize(Tree<T> tree){
        double[] result = new double[] {tree.getX(), tree.getY()};
        if(tree.getChild().size() > 0){
            tree.getChild().forEach(child->{
                double[] tempResult = treeSize(child);

                result[0] = Math.max(result[0], tempResult[0]);
                result[1] = Math.max(result[1], tempResult[1]);
            });
        }
        return result;
    }


    private String getParentAddress(String address){
        if(address.length() == 0) return null;

        String string = address.substring(0, address.length() - 1);
        int index = string.lastIndexOf("0");

        if(index > 0){
            return  string.substring(0, index + 1);
        } else {
            return "";
        }

    }

    @Override
    protected String getDialogFxml() {
        return "/fxml/routeDialog.fxml";
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    private void drawTree(Tree<Device> deviceDrawTree, Double parentX, Double parentY){
        DeviceTreeView deviceTreeView = new DeviceTreeView(ItemSize, ItemPadding);
        double myX = deviceDrawTree.getY();//*HorizontalSpacing;
        double myY = deviceDrawTree.getX();//*ItemSize;

        deviceTreeView.getContainer().setLayoutX(myX);
        deviceTreeView.getContainer().setLayoutY(myY);

        List<Sensor> deviceSensors = deviceDrawTree.getNode().getSensors();
        String id = "" + deviceSensors.get(0).getId();
        deviceTreeView.getText().setText(deviceSensors.size() > 1 ? id + ",..": id);

        if(deviceDrawTree.getChild().size() > 0) {
            deviceTreeView.getLeftText().setText(deviceDrawTree.getNode().getAddress());
        } else {
            deviceTreeView.getRightText().setText(deviceDrawTree.getNode().getAddress());
        }

        deviceContainer.getChildren().add(deviceTreeView.getContainer());

        if(parentX != null && parentY != null) {
            double[] points = new double[]{
                    parentX + ItemSize - ItemPadding,
                    parentY + ItemSize / 2,
                    myX + ItemPadding,
                    myY + ItemSize / 2
            };
            CubicCurve cubicCurve = new CubicCurve(
                    points[0],
                    points[1],
                    points[0] + (points[2] - points[0]) / 2,
                    points[1],
                    points[2] - (points[2] - points[0]) / 2,
                    points[3],
                    points[2],
                    points[3]
            );
            cubicCurve.setStroke(new Color(0.47, 0.47,0.47,1));
            cubicCurve.setFill(new Color(0,0,0,0));
            lineContainer.getChildren().add(cubicCurve);
        }

        deviceDrawTree.getChild().forEach(childTree-> drawTree(childTree, myX, myY));
    }
}
