package sample.dialog.route;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import sample.MainAppWindow;
import sample.models.Device;
import sample.util.AbsDialog;

import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by dimka on 5/26/2017.
 */
public class GenerationDialog extends AbsDialog implements Initializable {
    @FXML
    public ImageView imageView;
    @FXML
    public TextField startIdText, endIdText, routeIdText, radiusText;
    @FXML
    public Label generationNameNumber;


    int areaMinWidth;
    int areaMaxWidth;
    int areaMinHeight;
    int areaMaxHeight;

    int areaMinSensors;
    int areaMaxSensors;

    int holeMinWidth;// = Integer.parseInt(generationHoleMinWidth.textProperty().get());
    int holeMaxWidth;// = Integer.parseInt(generationHoleMaxWidth.textProperty().get());
    int holeMinHeight;// = Integer.parseInt(generationHoleMinHeight.textProperty().get());
    int holeMaxHeight;

    int generationCo;
    int currentGeneration = 0;
    MainAppWindow mainAppWindow;
    Random random;
    Date generationDate;

    private static String folderPath = "D:\\DataGeneration";
    private String generationPrefix;
    private List<int[]> hops;

    public GenerationDialog(int areaMinWidth, int areaMaxWidth, int areaMinHeight, int areaMaxHeight, int areaMinSensors, int areaMaxSensors, int holeMinWidth, int holeMaxWidth, int holeMinHeight, int holeMaxHeight, int generationCo, MainAppWindow mainAppWindow) {
        this.areaMinWidth = areaMinWidth;
        this.areaMaxWidth = areaMaxWidth;
        this.areaMinHeight = areaMinHeight;
        this.areaMaxHeight = areaMaxHeight;
        this.areaMinSensors = areaMinSensors;
        this.areaMaxSensors = areaMaxSensors;
        this.holeMinWidth = holeMinWidth;
        this.holeMaxWidth = holeMaxWidth;
        this.holeMinHeight = holeMinHeight;
        this.holeMaxHeight = holeMaxHeight;
        this.generationCo = generationCo;
        this.mainAppWindow = mainAppWindow;


        this.random = new Random(Calendar.getInstance().getTimeInMillis());
        this.hops = new ArrayList<>();

        generateField();

    }

    @Override
    protected String getDialogFxml() {
        return "/fxml/generationDialog.fxml";
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void nextGeneration(){
        try{
            int startId = Integer.parseInt(startIdText.textProperty().get());
            int endId = Integer.parseInt(endIdText.textProperty().get());
            int routeId = Integer.parseInt(routeIdText.textProperty().get());
            double radius = Integer.parseInt(radiusText.textProperty().get());

            List<Device> areaRoute = new ArrayList<>();
            List<Device> newRoute = new ArrayList<>();
            List<Device> newRouteFull = new ArrayList<>();

            mainAppWindow.createRings(startId, radius);
            WritableImage snapshot = mainAppWindow.areaGroup.snapshot(new SnapshotParameters(), null);
            saveWritableToFile(snapshot, generationPrefix + "_2_first_rings.png");

            mainAppWindow.createRings(endId, radius);
            snapshot = mainAppWindow.areaGroup.snapshot(new SnapshotParameters(), null);
            saveWritableToFile(snapshot, generationPrefix + "_3_second_rings.png");

            List<List<Device>> data = mainAppWindow.calculateAreaRoute(startId, endId, radius);
            for(List<Device> ringDevices : data){
                areaRoute.add(ringDevices.get(random.nextInt(ringDevices.size())));
            }

            snapshot = mainAppWindow.areaGroup.snapshot(new SnapshotParameters(), null);
            saveWritableToFile(snapshot, generationPrefix + "_4_area_route.png");

            mainAppWindow.createRings(routeId, radius);
            RouteShowDialog dialog =  mainAppWindow.calculateRoutingTable(routeId,radius);
            snapshot = dialog.mainContainer.snapshot(new SnapshotParameters(), null);
            saveWritableToFile(snapshot, generationPrefix + "_5_roting_table.png");
            dialog.close();

            newRoute = mainAppWindow.GetGFHSRoute(startId, endId,radius);

            mainAppWindow.drawRoute(areaRoute, newRoute);

            hops.add(new int[]{newRoute.size(), newRouteFull.size()});

            if(currentGeneration>= generationCo){
                File csvData = new File(folderPath + "\\" + generationPrefix + "_generation_result.csv");
                FileOutputStream fos = new FileOutputStream(csvData);
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

                int areaAllHops = hops.stream().map(hopss->hopss[0]).reduce(0, (integer, integer2) -> integer + integer2);
                int routeAllHops = hops.stream().map(hopss->hopss[1]).reduce(0, (integer, integer2) -> integer + integer2);

                bw.write(String.join(",",
                        IntStream.range(0, hops.size() + 2).mapToObj((i) -> {
                            if (i == 0) return "";
                            if (i == hops.size() + 1) return "All";
                            return "" + i;
                        }).collect(Collectors.toList())));
                bw.newLine();

                bw.write(String.join(",",
                        IntStream.range(0, hops.size() + 2).mapToObj((i) -> {
                            if (i == 0) return "H1";
                            if (i == hops.size() + 1) return "" + areaAllHops;
                            return "" + hops.get(i-1)[0];
                        }).collect(Collectors.toList())));
                bw.newLine();

                bw.write(String.join(",",
                        IntStream.range(0, hops.size() + 2).mapToObj((i) -> {
                            if (i == 0) return "H2";
                            if (i == hops.size() + 1) return "" + routeAllHops;
                            return "" + hops.get(i-1)[1];
                        }).collect(Collectors.toList())));
                bw.newLine();

                bw.write(String.join(",",
                        IntStream.range(0, hops.size() + 2).mapToObj((i) -> {
                            if (i == 0) return "";
                            if (i == hops.size() + 1) return "" + (double)areaAllHops/(double) routeAllHops;
                            return "" + (double)hops.get(i-1)[0]/ (double)hops.get(i-1)[1];
                        }).collect(Collectors.toList())));
                bw.newLine();


                bw.close();

                this.close();
                return;
            }
            generateField();

        } catch (Exception ignore){
            boolean a = false;
        }
    }

    public static List<Device> getRouteTree(List<Device> devices, Device device){
        String address = device.getAddress();
        List<Device> result = new ArrayList<>();

        while (address != null){
            result.add(findDeviceByAddress(devices, address));

            if(address.equalsIgnoreCase("")) break;
            address = address.substring(0, address.length()-1);
            int zeroLastIndex = address.lastIndexOf("0");

            if(zeroLastIndex < 0){
                address = "";
            } else {
                address = address.substring(0, zeroLastIndex+1);
            }
        }


        return result;
    }

    private static Device findDeviceByAddress( List<Device> devices, String address){
        return devices.stream().filter(device ->  device.getAddress().equalsIgnoreCase( address)).findFirst().orElse(null);
    }

    private int repeatCount(String original, String substr){
        int lastIndex = 0;
        int count = 0;

        while (lastIndex!= -1){
            lastIndex = original.indexOf(substr, lastIndex);
            if(lastIndex != -1){
                count++;
                lastIndex= lastIndex+substr.length();
            }
        }

        return count;
    }

    private void generateField(){
        int areaWidth = areaMinWidth + random.nextInt(areaMaxWidth - areaMinWidth);
        int areaHeight = areaMinHeight + random.nextInt(areaMaxHeight - areaMinHeight);
        mainAppWindow.createNewAreaInt(areaWidth, areaHeight,
                areaMinSensors + random.nextInt(areaMaxSensors - areaMinSensors));

        int border = 2;

        int holeWidth = holeMinWidth + random.nextInt(holeMaxWidth - holeMinWidth);
        int holeHeight = holeMinHeight + random.nextInt(holeMaxHeight - holeMinHeight);

        int offsetX = random.nextInt(areaWidth - 2*border - holeWidth);
        int offsetY = random.nextInt(areaHeight - 2*border - holeHeight);

        mainAppWindow.createHoleBound(offsetX,offsetY, offsetX + holeWidth, offsetY + holeHeight);

        WritableImage snapshot = mainAppWindow.areaGroup.snapshot(new SnapshotParameters(), null);
        imageView.setImage(snapshot);
        imageView.setFitHeight(snapshot.getHeight());
        imageView.setFitWidth(snapshot.getWidth());
        imageView.setPreserveRatio(true);
        if(generationDate == null) generationDate =Calendar.getInstance().getTime();

        generationPrefix ="generation_" +  new SimpleDateFormat("yyyyMMddHHmmss").format(generationDate) + "_"+currentGeneration;

        saveWritableToFile(snapshot,  generationPrefix + "_1_main.png");
        currentGeneration++;
    }

    private void saveWritableToFile(WritableImage snapshot, String fileName) {
        File file = new File(folderPath + "\\" + fileName);
        RenderedImage renderedImage = SwingFXUtils.fromFXImage(snapshot, null);
        try {
            ImageIO.write(
                    renderedImage,
                    "png",
                    file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
