package sample.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimka on 3/4/2017.
 */
public class Rings {
    private List<List<Integer>> rings;

    public Rings() {
        this.rings = new ArrayList<>();
    }

    public void clear(){
        this.rings.clear();
    }

    public List<List<Integer>> getRings() {
        return rings;
    }

    public List<Integer> getLastRing() {
        return getRing(rings.size() - 1);
    }

    public List<Integer> getRing(int i) {
        if (i >= 0 && i < rings.size()) {
            return rings.get(i);
        }
        return null;
    }

    public boolean isEmpty() {
        return rings.isEmpty();
    }

    public int getSize() {
        return rings.size();
    }

    public void addRing(List<Integer> ring) {
        if (!rings.contains(ring)) {
            for (List<Integer> oldRing : rings) {
                ring.removeAll(oldRing);

            }
            rings.add(ring);
        }
    }
}
