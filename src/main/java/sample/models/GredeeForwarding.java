package sample.models;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dimka on 9/27/2017.
 */
public class GredeeForwarding {
    /**
     * Forward packet from the source to the destination using ordinar Greedy Forwarding based on the local obstacles knowledge
     *
     * @param src - source node
     * @param dst - destination node
     * @param t - topology
     * @param rightHandRule - if true than use perimeter mode for local minimum recovery (i.e., use GPSR)
     * @param ttl - the packet's TTL
     * @return Resulting path
     */

    private static double d(Device left, Device right){
        double a1 = (left.getX() - right.getX());
        double a2 = (left.getY() - right.getY());
        return Math.sqrt(a1 * a1 + a2 * a2);
    }

    private static double d(double x1, double y1, double x2, double y2){
        double a1 = (x1 - x2);
        double a2 = (y1 - y2);
        return Math.sqrt(a1 * a1 + a2 * a2);
    }
    public static List<Device> greedyForwarding(int src, int dst,double radius, List<Device> devices, boolean rightHandRule, int ttl)
    {
        List<Device> path = new ArrayList<>();
        ;
        path.add(Device.getDeviceBySensorId(devices, src));
        Device dstS = Device.getDeviceBySensorId(devices, dst);

        while (path.get(path.size() - 1) != dstS && path.size() < ttl)
        {
            Device n = path.get(path.size() - 1);
            Device next = null;
            double min = d(n, dstS);

            for (Device neighbor : Device.getDeviceNeighborhoods(devices, n, radius, false).collect(Collectors.toList()) )
                if (d(neighbor, dstS) < min)                 {
                    next = neighbor;
                    min = d(neighbor, dstS);
                }

            if (next == null) //indicates local minimum
                if (rightHandRule)
                    try
                    {
                        perimeterModeForwarding(path, dstS, ttl, devices, radius);
                    } catch (Exception e) //indicates absence of neighbors for n (disconnected node)
                    {
                        System.out.println(e.toString());
                        return path;
                    }
                else
                {
                    System.out.println("GF faced local minimum!");
                    return path;
                }
            else
                path.add(next);
        }

        if (path.get(path.size() - 1) == dstS)
            return path;
        else
            return path;
    }

    /**
     * This method forwards packet in a Perimeter mode of GPSR (when possible returns back to Greedy Forwarding mode)
     * @param path - path traversed by a packet so far
     * @param dstN - destination node
     * @param ttl - packet's TTL
     */
    private static void perimeterModeForwarding(List<Device> path, Device dstN, int ttl,  List<Device> device, double radius) throws Exception
    {
        Device Lp = null;
        List<Double> Lf = new ArrayList<>(2);
        List<Device> e0 = new ArrayList<>(2);

        while (path.get(path.size() - 1) != dstN && path.size() < ttl)
        {
            Device n = path.get(path.size() - 1);
            Device next = null;
            if (Lp == null)
            {
                //set-up initial packet fields
                Lp = n;
                Lf.clear();
                Lf.add(Double.valueOf(n.getX()));
                Lf.add(Double.valueOf(n.getY()));

                next = periInitForward2(n, dstN, radius, device);

                //set-up first visited edge on the face
                e0.clear();
                e0.add(n);
                e0.add(next);
            } else
            {
                next = rightHandForward2(n, path.get(path.size() - 2), device, radius);

                if (!e0.isEmpty() && n.equals(e0.get(0)) && next.equals(e0.get(1)))
                {
                    //System.out.println("Exception 1: src=" + e0.get(0).getId() + " dst=" + e0.get(1).getId() + " n=" + n.getId() + "n=" + next.getId());
                    throw new Exception("Perimeter Forwarding went through the same edge in the same direction. Terminate!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                } else
                {

                    next = faceChange(n, next, Lp, dstN, Lf, e0, device, radius);
                }
            }

            if (next == null)
                throw new Exception("Perimeter Forwarding didn't found next hop!");

            path.add(next);

            if (d(next, dstN) <d(Lp, dstN)) //return to GF
                return;
        }
    }

    /**
     * internal method for GPSR perimeter mode
     * @param n
     * @param dstX
     * @param dstY
     * @return
     */
    private static Device periInitForward2(Device n, Device dst, double radius, List<Device> devices)
    {
        double nDist = d(n,dst);// EuclDist.d(n.getX(), n.getY(), dstX, dstY);

        Device minne = null;
        double min = Double.MAX_VALUE;

        for (Device ne : Device.getDeviceNeighborhoods(devices, n, radius, false).collect(Collectors.toList()))
        {
                double neDist = d(n, ne);
                double angle = Math.acos(((dst.getX() - n.getX()) * (ne.getX() - n.getX())
                        + (dst.getY() - n.getY()) * (ne.getY() - n.getY())) / (nDist * neDist)); // cos(q)= a*b/|a||b|
                if (!isNeighborOnLeftSide(ne, n, dst.getX(), dst.getY()))
                    angle = 2 * Math.PI - angle;

                if (angle < min) //at least one sensor will be found
                {
                    minne = ne;
                    min = angle;
                }
            }
        return minne;
    }

    /**
     * internal method for GPSR perimeter mode
     * @param n
     * @param inne
     * @return
     */
    private static Device rightHandForward2(Device n, Device inne, List<Device> devices,  double radius)
    {
        double nDist = d(n, inne);

        Device minne = null;
        double min = Double.MAX_VALUE;

        for (Device ne : Device.getDeviceNeighborhoods(devices, n, radius, false).collect(Collectors.toList()))
            if (!ne.equals(inne))
            {
                double neDist = d(n, ne);
                double angle = Math.acos(((inne.getX() - n.getX()) * (ne.getX() - n.getX())
                        + (inne.getY() - n.getY()) * (ne.getY() - n.getY())) / (nDist * neDist)); // cos(q)= a*b/|a||b|
                if (!isNeighborOnLeftSide(ne, n, inne))
                    angle = 2 * Math.PI - angle;

                if (angle < min) //at least one sensor will be found
                {
                    minne = ne;
                    min = angle;
                }
            }

        if (minne == null)
            return inne;
        else
            return minne;
    }

    /**
     * internal method for GPSR perimeter mode
     * @param n
     * @param next
     * @param Lp
     * @param dstS
     * @param Lf
     * @param e0
     * @return
     */
    private static Device faceChange(Device n, Device next, Device Lp, Device dstS, List<Double> Lf, List<Device> e0, List<Device> devices, double radius)
    {
        List<Double> intersection = lineCrossed(Lp, dstS, n, next);
        if (!intersection.isEmpty() &&
                d(intersection.get(0), intersection.get(1), dstS.getX(), dstS.getY())
                        < d(Lf.get(0), Lf.get(1), dstS.getX(), dstS.getY()))
        {
            Lf = intersection;
            next = rightHandForward2(n, next, devices, radius);
            next = faceChange(n, next, Lp, dstS, Lf, e0, devices, radius);
            e0.clear();
            e0.add(n);
            e0.add(next);
        }

        return next;
    }

    /**
     * internal method for GPSR perimeter mode
     * @param Lp
     * @param dstS
     * @param s
     * @param next
     * @return
     */
    private static List<Double> lineCrossed(Device Lp, Device dstS, Device s, Device next)
    {
        double[] dy = new double[2];
        double[] dx = new double[2];
        double[] m = new double[2];
        double[] b = new double[2];
        double xint, yint;
        List<Double> intersection = new ArrayList<>(2);

        double x1 = Lp.getX();
        double y1 = Lp.getY();
        double x2 = dstS.getX();
        double y2 = dstS.getY();
        double x3 = s.getX();
        double y3 = s.getY();
        double x4 = next.getX();
        double y4 = next.getY();

        dy[0] = y2 - y1;
        dx[0] = x2 - x1;
        dy[1] = y4 - y3;
        dx[1] = x4 - x3;
        m[0] = dy[0] / dx[0];
        m[1] = dy[1] / dx[1];
        b[0] = y1 - m[0] * x1;
        b[1] = y3 - m[1] * x3;
        if (m[0] != m[1])
        {
            // slopes not equal, compute intercept
            xint = (b[0] - b[1]) / (m[1] - m[0]);
            yint = m[1] * xint + b[1];
            // is intercept in both line segments?
            if ((xint <= Math.max(x1, x2)) && (xint >= Math.min(x1, x2)) &&
                    (yint <= Math.max(y1, y2)) && (yint >= Math.min(y1, y2)) &&
                    (xint <= Math.max(x3, x4)) && (xint >= Math.min(x3, x4)) &&
                    (yint <= Math.max(y3, y4)) && (yint >= Math.min(y3, y4)))
            {
                intersection.add(xint);
                intersection.add(yint);
            }
        }
        return intersection;
    }

    /**
     * internal method for GPSR perimeter mode
     * @param ne
     * @param s
     * @param inne
     * @return
     */
    private static boolean isNeighborOnLeftSide(Device ne, Device s, Device inne)
    {
        return isNeighborOnLeftSide(ne, s, inne.getX(), inne.getY());
    }

    /**
     * internal method for GPSR perimeter mode
     * @param ne
     * @param s
     * @param dstX
     * @param dstY
     * @return
     */
    private static boolean isNeighborOnLeftSide(Device ne, Device s, double dstX, double dstY) //move counterclock-wise
    {
        if (((dstX - s.getX()) * (ne.getY() - s.getY())
                - (dstY - s.getY()) * (ne.getX() - s.getX())) >= 0)
            return true;
        else
            return false;
    }

    /* code from the GPSR Thesis Work
        private Node periInitForward(Node s, double dstX, double dstY)
    {
        // find bearing to dst
        double basebrg = bearing(dstX, dstY, s.getX(), s.getY());
        // find neighbor with greatest bearing <= brg
        Node minne = null;
        double brg, minbrg = 3 * Math.PI;
        for (Node ne : s.getNeighbors())
        {
            if (s.isProhibited(ne))
                continue;
            brg = bearing(ne.getX(), ne.getY(), s.getX(), s.getY()) - basebrg;
            //normilize brg
            if (brg < 0)
                brg += 2 * Math.PI;
            if (brg < 0)
                brg += 2 * Math.PI;
            if (brg < minbrg)
            {
                minbrg = brg;
                minne = ne;
            }
        }
        return minne;
    }
    private Node rightHandForward(Node s, Node inne)
    {
        double basebrg;
        // find bearing from mn to (x, y, z)
        basebrg = bearing(inne.getX(), inne.getY(), s.getX(), s.getY());
        Node minne = null;
        double brg, minbrg = 3 * Math.PI;
        for (Node ne : s.getNeighbors())
        {
            if (ne.equals(inne))
                continue;
            if (s.isProhibited(ne))
                continue;
            brg = bearing(s.getX(), s.getY(), ne.getX(), ne.getY()) - basebrg;
            //normilize brg
            if (brg < 0)
                brg += 2 * Math.PI;
            if (brg < 0)
                brg += 2 * Math.PI;
            if (brg < minbrg)
            {
                minbrg = brg;
                minne = ne;
            }
        }
        if (minne == null)
            return inne;
        else
            return minne;
    }
    private double bearing(double x1, double y1, double x2, double y2)
    {
        double brg;
        // XXX only deal with 2D for now
        // XXX check for (0, 0) args, a domain error
        brg = Math.atan2(y2 - y1, x2 - x1);
        if (brg < 0)
            brg += 2 * Math.PI;
        return brg;
    }
     */
}
