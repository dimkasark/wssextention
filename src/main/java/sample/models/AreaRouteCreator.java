package sample.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by dimka on 5/25/2017.
 */
public class AreaRouteCreator {

    private List<Sensor> sensors;
    private List<Device> devices;

    public AreaRouteCreator(List<Sensor> sensors, List<Device> devices) {
        this.sensors = sensors;
        this.devices = devices;
    }

    public List<List<Device>> calculateRoutes(int startId, int endId, double radius){
        RingCreator creator = new RingCreator(sensors, devices);

        List<List<Integer>> firstSensorRings = creator.createRings(startId, endId, radius).getRings();
        List<List<Integer>> secondSensorRings = creator.createRings(endId, startId, radius).getRings();

        return IntStream.range(0, firstSensorRings.size()).mapToObj((i)->{
            List<Integer> fromRing;

            if(i == 0){
                fromRing = new ArrayList<>();
                fromRing.add(startId);
            } else if( i == firstSensorRings.size()-1){
                fromRing = new ArrayList<>();
                fromRing.add(endId);
            } else {
                fromRing = firstSensorRings.get(i);
            }
            List<Integer> secondRing = secondSensorRings.get(firstSensorRings.size() - 1 - i);

            return fromRing.stream().filter(secondRing::contains).map((sensorId)-> Device.getDeviceBySensorId(devices, sensorId)).collect(Collectors.toList());
        }).collect(Collectors.toList());
    }

    public List<List<Device>> findWays(int startId, int endId, double radius){
        RingCreator creator = new RingCreator(sensors, devices);

        List<List<Integer>> firstSensorRings = creator.createRings(startId, endId, radius).getRings();
        firstSensorRings.remove(firstSensorRings.size() - 1);
        firstSensorRings.add(Collections.singletonList(endId));

        return createCartesianProduct(firstSensorRings,radius, Integer.MAX_VALUE);
    }

    private List<List<Device>> createCartesianProduct(final List<List<Integer>> lists, double distance, final int maxRouteLimit) {
        switch (lists.size()) {
            case 0:
                return new ArrayList<>();
            case 1:
                return new ArrayList<List<Device>>() {
                    {
                        for (int i = 0; i < (lists.get(0).size() <= maxRouteLimit ? lists.get(0).size() : maxRouteLimit); i++) {
                            add(Collections.singletonList(Device.getDeviceBySensorId(devices, lists.get(0).get(i))));
                        }
                    }
                };
            default:
                return createCartesianProductInternal(0, lists, distance, maxRouteLimit);
        }
    }

    private List<List<Device>> createCartesianProductInternal(int index, final List<List<Integer>> lists, double distance, int maxRouteLimit) {
        List<List<Device>> ret = new ArrayList<>();
        if (index == lists.size()) {
            ret.add(new ArrayList<>());
        } else {
            List<List<Device>> q = createCartesianProductInternal(index + 1, lists, distance, maxRouteLimit);
            for (final Integer obj : lists.get(index)) {
                Device newObj = Device.getDeviceBySensorId(devices, obj);
                List<Device> neighbors
                        = Device.getDeviceNeighborhoods(devices, Device.getDeviceBySensorId(devices, obj), distance).collect(Collectors.toList());
                for (final List<Device> list : q) {
                    if (ret.size() >= maxRouteLimit) {
                        break;
                    }
                    if (!list.isEmpty() && !neighbors.contains(list.get(0))) {
                        continue;
                    }
                    ret.add(new ArrayList<Device>() {  {
                            add(newObj);
                            addAll(list);
                        }
                    });
                }
            }
        }
        return ret;
    }
}
