package sample.models;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dimka on 3/4/2017.
 */
public class RingCreator {
    private List<Sensor> sensors;
    private List<Device> devices;

    public RingCreator(List<Sensor> sensors, List<Device> devices) {
        this.sensors = sensors;
        this.devices = devices;
    }

    public Rings createRings(int startSensorId, int endSensorId, double distance) throws RuntimeException {
        if (!isSensorContains(startSensorId)) {
            throw new RuntimeException("Sensors not contained start element with id:" + startSensorId);
        } else if (!isSensorContains(endSensorId)) {
            throw new RuntimeException("Sensors not contained end element with id:" + endSensorId);
        } else {
            Sensor sensor = getSensorById(endSensorId);
            Device device = devices.stream().filter(arrayDevice -> arrayDevice.getY() == sensor.getY() && arrayDevice.getX() == sensor.getX()).findFirst().orElse(null);
            return createRingsInt(startSensorId, device.getSensors().get(0), Math.abs(distance), new Rings());
        }
    }

    public Rings createRingsForCentralPoint(int startSensorId, double distance) {
        return createRingsInt(startSensorId, null, distance, new Rings());
    }

    private Rings createRingsInt(int startSensorId, Sensor endSensor, double distance, Rings rings) {
        if (rings.isEmpty()) {
            rings = addRootRing(startSensorId, distance, rings);
        } else {
            rings = addNextRing(distance, rings);
        }
        if (endSensor != null) {
            if (rings.getLastRing().contains(endSensor.getId())) {
                return rings;
            }
        }
        if (rings.getLastRing().isEmpty()) {
            rings.getRings().remove(rings.getLastRing());
            return rings;
        } else {
            for(Integer sensorId : rings.getLastRing()){
                Device.getDeviceBySensorId(devices, sensorId).setRingNumber(rings.getSize());
            }
        }
        return createRingsInt(startSensorId, endSensor, distance, rings);
    }

    private Rings addRootRing(int startSensorId, double distance, Rings rings) {
        rings.addRing(getListSensorsIds(getSensorNeighborhoods(getSensorById(startSensorId), distance)));
        return rings;
    }

    private Rings addNextRing(double distance, Rings rings) {
        List<Integer> ring = new ArrayList<>();
        for (Integer i : rings.getLastRing()) {
            for (Integer j : getListSensorsIds(getSensorNeighborhoods(getSensorById(i), distance))) {
                if (!ring.contains(j)) {
                    ring.add(j);
                }
            }
        }
        rings.addRing(ring);
        return rings;
    }

    private List<Integer> getListSensorsIds(List<Sensor> sensors) {
        return  sensors.stream().map(Sensor::getId).collect(Collectors.toList());
    }

    private boolean isSensorContains(int sensorId){
        return sensors.stream().filter((sensor -> sensorId == sensor.getId())).count() > 0;
    }

    private List<Sensor> getSensorNeighborhoods(Sensor sensor, double distance){
        return devices.stream()
                .filter(arraySensor ->{
                            double deltaX = sensor.getX() - arraySensor.getX();
                            double deltaY = sensor.getY() - arraySensor.getY();
                            return  Math.abs(deltaX) <= distance &&
                                    Math.abs(deltaY) <= distance &&
                                    Math.sqrt(deltaX * deltaX + deltaY * deltaY) <= distance;
                        })
                .map(device-> device.getSensors().get(0))
                .collect(Collectors.toList());
    }

    private Sensor getSensorById(int sensorId) {
        return sensors.stream().filter(sensor -> sensorId == sensor.getId()).findFirst().orElse(null);
    }
}
