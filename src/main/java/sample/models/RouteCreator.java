package sample.models;

import javafx.util.Pair;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.IntConsumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by dimka on 3/4/2017.
 */
public class RouteCreator {
    private List<Device> devices;

    public RouteCreator(List<Device> devices) {
        this.devices = devices;
    }

    public void calculateRoutingTable(Device device, double distance) {
        Queue<Pair<Device, List<Device>>> routingQueue = new LinkedList<>();

        do {
            Pair<Device, List<Device>> routingDevice;
            if(routingQueue.isEmpty()){
                /**first load*/
                routingDevice = new Pair<>(device, Device.getDeviceNeighborhoods(devices, device, distance).collect(Collectors.toList()));
            } else {
                routingDevice = routingQueue.poll();
            }

            List<Pair<Device, List<Device>>> deviceUnRoutedChilds = routingDevice.getValue()
                    .stream()
                    .filter(child -> child.getAddress() == null)
                    .map(neighborDevice -> new Pair<>(neighborDevice, Device.getDeviceNeighborhoods(devices, neighborDevice, distance, true).collect(Collectors.toList())))
                    .sorted((lht, rht)-> Integer.compare(rht.getValue().size(), lht.getValue().size()))
                    .collect(Collectors.toList());

            IntStream.range(0, deviceUnRoutedChilds.size()).forEach(value -> {
                Pair<Device, List<Device>> neighborDevice = deviceUnRoutedChilds.get(value);

                neighborDevice.getKey().setAddress(routingDevice.getKey().getAddress() + repeat("1",value) + "10" );
                if(routingDevice.getValue().size() > 0){
                    routingQueue.add(neighborDevice);
                }
            });


        } while (!routingQueue.isEmpty());

    }

    private String repeat(String main, int count){
        if(count <= 0) return "";
        StringBuilder builder = new StringBuilder(main);
        while (--count > 0){
            builder.append(main);
        }

        return builder.toString();
    }
}
