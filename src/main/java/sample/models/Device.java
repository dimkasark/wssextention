package sample.models;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by dimka on 3/4/2017.
 */
public class Device {
    private List<Sensor> sensors;
    private double x;
    private double y;
    private int ringNumber;

    private String address;

    public Device(double x, double y) {
        this.x = x;
        this.y = y;
        this.sensors = new ArrayList<>();
        ringNumber = 0;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Sensor> getSensors() {
        return sensors;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getRingNumber() {
        return ringNumber;
    }

    public void setRingNumber(int ringNumber) {
        this.ringNumber = ringNumber;
    }

    @Override
    public String toString() {
        return address + " { x: '"+getX()+"', y: '"+getY()+"'}";
    }

    public static Device getDeviceBySensorId(List<Device> devices, int sensorId){
        return devices.stream().filter((device ->
                device.getSensors().stream().filter(sensor -> sensor.getId() == sensorId).findAny().orElse(null)!= null)).findAny().orElse(null);
    }

    public static Stream<Device> getDeviceNeighborhoods(List<Device> devices, Device device, double distance) {
        return getDeviceNeighborhoods(devices, device, distance, false);
    }

    public static Stream<Device> getDeviceNeighborhoods(List<Device> devices, Device device, double distance, boolean onlyNextRing) {
        return devices.stream().filter(arraySensor -> {
            double deltaX = device.getX() - arraySensor.getX();
            double deltaY = device.getY() - arraySensor.getY();
            return arraySensor != device &&
                    Math.abs(deltaX) <= distance &&
                    Math.abs(deltaY) <= distance &&
                    Math.sqrt(deltaX * deltaX + deltaY * deltaY) <= distance &&
                    (!onlyNextRing || device.getRingNumber() < arraySensor.getRingNumber());
        });
    }
}
