package sample.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dimka on 9/28/2017.
 */
public class GF2 {

    private static double d(Device left, Device right){
        double a1 = (left.getX() - right.getX());
        double a2 = (left.getY() - right.getY());
        return Math.sqrt(a1 * a1 + a2 * a2);
    }

    private static double d(double x1, double y1, double x2, double y2){
        double a1 = (x1 - x2);
        double a2 = (y1 - y2);
        return Math.sqrt(a1 * a1 + a2 * a2);
    }

    public static List<Device> findRouteByGPSR(int startSensorId, int endSensorId, double distance, List<Device> devices)
    {
        Device source = Device.getDeviceBySensorId(devices, startSensorId);
        Device dst = Device.getDeviceBySensorId(devices, endSensorId);

        List<Device> way = findRouteByGPSRInt(source, dst, distance, new ArrayList<>(), devices);

        way.add(0, source);
        way.add(dst);
        return way;
    }

    private static List<Device> findRouteByGPSRInt(Device src, Device dst, double distance, List<Device> way, List<Device> devices)
    {

        List<Device> firstNeighbourhood = Device.getDeviceNeighborhoods(devices, src, distance, false).collect(Collectors.toList());//  ringsCreator.getNeighborhoodsForSensor(srcId, distance);
        //List firstNeighbourhoodIds = ringsCreator.getListSensorsIds(firstNeighbourhood);

        way.add(src);
        if(firstNeighbourhood.contains(dst)) {
            way.remove(0);
            return way;
        }
        double geographicDistance =  d(src, dst);
        Device newSrcId = null;

        for (Device device1: firstNeighbourhood) {
            double newGeoDist = d(device1, dst);//  area.geographicDistance(id.intValue(), destId);
            if(newGeoDist < geographicDistance)             {
                geographicDistance = newGeoDist;
                newSrcId = device1;
            }
        }

        if(newSrcId == null)
        {
            List<Device> perimetrWay = findRouteByPerimetrForwarding(src, dst, distance, new ArrayList<>(), devices);
            newSrcId = perimetrWay.get(perimetrWay.size() - 1);
            perimetrWay.remove(perimetrWay.size() - 1);
            way.addAll(perimetrWay);
        }
        return findRouteByGPSRInt(newSrcId, dst, distance, way, devices);
    }

    private static List<Device> findRouteByPerimetrForwarding(Device srcId, Device destId, double distance, List<Device> way, List<Device> devices)
    {
        List<Device> firstNeighbourhood = Device.getDeviceNeighborhoods(devices, srcId, distance, false).collect(Collectors.toList());//ringsCreator.getNeighborhoodsForSensor(srcId, distance);
        //List firstNeighbourhoodIds = ringsCreator.getListSensorsIds(firstNeighbourhood);
        if(firstNeighbourhood.contains(destId)) {
            way.remove(0);
            return way;
        }

        double angle = 6.2831853071795862D;
        List<Device> next = new ArrayList<>();
        Device prev = way.isEmpty() ? destId : way.get(way.size() - 1);
        //Sensor prev = sensors.getSensorById(way.isEmpty() ? destId : ((Integer)way.get(way.size() - 1)).intValue());
        //Iterator i$ = firstNeighbourhood.iterator();

        for (Device device1: firstNeighbourhood) {
            double newAngle = calculateAngle(srcId, prev, device1);
            if(newAngle < angle)             {
                angle = newAngle;
                next = new ArrayList<>();
                next.add(device1);
            } else if(newAngle == angle)
                next.add(device1);
        }

        Device newSensor = getSensorWithMaxVectorLength(next, srcId);
        way.add(srcId);
        if(d(newSensor, destId) < d(srcId, destId))         {
            way.add(newSensor);
            way.remove(0);
            return way;
        } else         {
            return findRouteByPerimetrForwarding(newSensor, destId, distance, way, devices);
        }
    }

    private static Device getSensorWithMaxVectorLength(List<Device> next, Device src)
    {
        double max = 0.0D;
        Device newSensor = null;

        for (Device device: next) {
            double length = Math.sqrt(Math.pow(device.getX() - src.getX(), 2D) + Math.pow(device.getY() - src.getY(), 2D));
            if(length > max) {
                newSensor = device;
                max = length;
            }
        }

        if(next.isEmpty() || newSensor == null)
            throw new RuntimeException((new StringBuilder()).append("Wrong vector length calculation for ").append(src.getSensors().get(0).getId()).toString());
        else
            return newSensor;
    }

    private static double calculateAngle(Device src, Device dst, Device next)
    {
        double alpha = Math.acos((double)((dst.getX() - src.getX()) * (next.getX() - src.getX()) + (dst.getY() - src.getY()) * (next.getY() - src.getY())) / (Math.sqrt(Math.pow(dst.getX() - src.getX(), 2D) + Math.pow(dst.getY() - src.getY(), 2D)) * Math.sqrt(Math.pow(next.getX() - src.getX(), 2D) + Math.pow(next.getY() - src.getY(), 2D))));
        if((dst.getX() - src.getX()) * (next.getY() - src.getY()) - (dst.getY() - src.getY()) * (next.getX() - src.getX()) >= 0)
            alpha = 6.2831853071795862D - alpha;
        return alpha;
    }

    /*private List removeAllUnEqualsElements(List first, List second)
    {
        List result = new ArrayList();
        Iterator i$ = second.iterator();
        do
        {
            if(!i$.hasNext())
                break;
            Integer i = (Integer)i$.next();
            if(first.contains(i))
                result.add(i);
        } while(true);
        return result;
    }*/

}
