package sample.util;

import java.util.List;

/**
 * Created by dimka on 3/17/2017.
 */
public class TreeNode<T> {
    private T node;
    private List<TreeNode<T>> childs;

    public TreeNode(T node, List<TreeNode<T>> childs) {
        this.node = node;
        this.childs = childs;
    }

    public T getNode() {
        return node;
    }


    public List<TreeNode<T>> getChilds() {
        return childs;
    }
}
