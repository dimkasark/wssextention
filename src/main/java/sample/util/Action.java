package sample.util;

/**
 * Created by dimka on 3/10/2017.
 */
public interface Action<Parameter> {
    void action(Parameter parameter);
}
