package sample.util;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;

/**
 * Created by dimka on 3/10/2017.
 */
public class ItemView {
    private StackPane container;
    private Circle circle;
    private Text text;

    private double oldSize;

    public ItemView(String id, double size) {
        oldSize = size;
        this.container = new StackPane();
        this.container.setMaxSize(size,size);
        this.container.setMinSize(size,size);

        this.circle = new Circle(size/2.0 - 2);
        this.circle.setStroke(new Color(0,0,0,1));
        this.circle.setStrokeWidth(1);
        this.circle.setFill(new Color(0,0,0,0));

        this.text = new Text(id);
        this.text.setBoundsType(TextBoundsType.VISUAL);
        this.text.setFont(Font.font("Arial", 15));

        this.container.getChildren().addAll(this.circle, this.text);
    }

    public void changeSize(double size){
        this.container.setMaxSize(size,size);
        this.container.setMinSize(size,size);
        this.circle.setRadius(size/2.0 - 2);

        double diff = size - oldSize;
        this.container.setLayoutX(this.container.getLayoutX() - diff/2);
        this.container.setLayoutY(this.container.getLayoutY() - diff/2);

        oldSize = size;
    }

    public StackPane getContainer() {
        return container;
    }

    public Circle getCircle() {
        return circle;
    }

    public Text getText() {
        return text;
    }
}
