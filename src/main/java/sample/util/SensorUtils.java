package sample.util;

import sample.models.Device;
import sample.models.Sensor;

import java.util.*;

/**
 * Created by dimka on 3/4/2017.
 */
public class SensorUtils {
    public static List<Sensor> generateField(int maxX, int maxY, int sensorsCount) {
        ArrayList<Sensor> result = new ArrayList<>(sensorsCount);
        Random random = new Random(Calendar.getInstance().getTimeInMillis());
        for (int i = 0; i < sensorsCount; i++) {
            result.add(new Sensor(random.nextInt(maxX), random.nextInt(maxY), i + 1));
        }
        return result;
    }

    public static List<Device> generateDevices(List<Sensor> sensors){
        List<Sensor> copy = new ArrayList<>(sensors);

        ArrayList<Device> devices = new ArrayList<>();

        int index = 0 ;
        while(copy.size() > 0){
            Sensor sensor = copy.get(0);
            Device newDevice = new Device(sensor.getX(), sensor.getY());
            newDevice.getSensors().add(sensor);

            copy.remove(sensor);

            for (int j = index; j< sensors.size(); j++){
                Sensor sameSensor = sensors.get(j);

                if(sameSensor.getX() == sensor.getX() && sameSensor.getY() == sensor.getY() && copy.contains(sameSensor)){
                    newDevice.getSensors().add(sameSensor);
                    copy.remove(sameSensor);
                }
            }
            index++;
            devices.add(newDevice);
        }

        return devices;
    }

    public static void createHoleInArea(List<Device> area, int minX, int minY, int maxX, int maxY, RemoveHandler<Device> handler){
        int index = 0;
        while (area.size() > index){
            Device device = area.get(index);

            if(inRange(device.getX(), minX, maxX) && inRange(device.getY(), minY, maxY)){
                area.remove(device);
                handler.onItemRemoved(device);
            } else{
                index++;
            }
        }
    }

    private static boolean inRange(double value, double min, double max){
        return  value >= min && value <=max;
    }

    public interface RemoveHandler<T>{
        void onItemRemoved(T item);
    }


}
