package sample.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimka on 3/4/2017.
 */
public class Device {
    private List<Sensor> sensors;
    private double x;
    private double y;

    private String address;

    public Device(double x, double y) {
        this.x = x;
        this.y = y;
        this.sensors = new ArrayList<>();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Sensor> getSensors() {
        return sensors;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public String toString() {
        return address + " { x: '"+getX()+"', y: '"+getY()+"'}";
    }
}
