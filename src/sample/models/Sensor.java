package sample.models;

/**
 * Created by dimka on 3/4/2017.
 */
public class Sensor {
    private double x;
    private double y;
    private int id;

    public Sensor(double x, double y, int id) {
        this.x = x;
        this.y = y;
        this.id = id;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getId() {
        return id;
    }
}
