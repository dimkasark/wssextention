package sample.models;

import javafx.util.Pair;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.IntConsumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by dimka on 3/4/2017.
 */
public class RouteCreator {
    private List<Device> devices;

    public RouteCreator(List<Device> devices) {
        this.devices = devices;
    }

    public void calculateRoutingTable(Device device, double distance) {
        Queue<Pair<Device, List<Device>>> routingQueue = new LinkedList<>();

        do {
            Pair<Device, List<Device>> routingDevice;
            if(routingQueue.isEmpty()){
                /**first load*/
                routingDevice = new Pair<>(device, getDeviceNeighborhoods(device, distance).collect(Collectors.toList()));
            } else {
                routingDevice = routingQueue.poll();
            }

            int index = 0;
            List<Pair<Device, List<Device>>> deviceUnRoutedChilds = routingDevice.getValue()
                    .stream()
                    .filter(child -> child.getAddress() == null)
                    .map(neighborDevice -> new Pair<>(neighborDevice, getDeviceNeighborhoods(neighborDevice, distance).collect(Collectors.toList())))
                    .sorted(Comparator.comparingInt(o -> o.getValue().size()))
                    .collect(Collectors.toList());

            IntStream.range(0, deviceUnRoutedChilds.size()).forEach(value -> {
                Pair<Device, List<Device>> neighborDevice = deviceUnRoutedChilds.get(value);

                neighborDevice.getKey().setAddress(routingDevice.getKey().getAddress() + repeat("1",value) + "10" );
                if(routingDevice.getValue().size() > 0){
                    routingQueue.add(neighborDevice);
                }
            });


        } while (!routingQueue.isEmpty());

    }

    private Stream<Device> getDeviceNeighborhoods(Device device, double distance) {
        return devices.stream().filter(arraySensor -> {
            double deltaX = device.getX() - arraySensor.getX();
            double deltaY = device.getY() - arraySensor.getY();
            return Math.abs(deltaX) <= distance &&
                    Math.abs(deltaY) <= distance &&
                    Math.sqrt(deltaX * deltaX + deltaY * deltaY) <= distance;
        });
    }

    private String repeat(String main, int count){
        if(count <= 0) return "";
        StringBuilder builder = new StringBuilder(main);
        while (--count > 0){
            builder.append(main);
        }

        return builder.toString();
    }
}
