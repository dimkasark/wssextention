package sample.models;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import java.util.List;

/**
 * Created by dimka on 3/10/2017.
 */
public class DeviceRing {
    private List<Device> devices;
    private boolean isDraw;
    private Paint ringPaint;
    private int ringNumber;

    public DeviceRing(List<Device> devices, int ringNumber) {
        this.devices = devices;
        this.ringNumber = ringNumber;
        isDraw = false;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setRingPaint(Paint ringPaint) {
        this.ringPaint = ringPaint;
    }

    public boolean isDraw() {
        return isDraw;
    }

    public void setDraw(boolean draw) {
        isDraw = draw;
    }

    public Paint getRingPaint() {
        return isDraw ? ringPaint : new Color(0,0,0,0);
    }

    public int getRingNumber() {
        return ringNumber;
    }
}
