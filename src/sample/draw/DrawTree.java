package sample.draw;

import sample.util.TreeNode;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by dimka on 3/17/2017.
 */
public class DrawTree<T> {
    private double x;
    private double y;
    private TreeNode<T> tree;
    private DrawTree<T> parent;
    private List<DrawTree<T>> childs;
    private DrawTree<T> thread;
    private double offset;
    private DrawTree<T> ancestor;
    private double change;
    private double shift;
    private double mod;
    private DrawTree<T> _lmostSibling;
    private int number;

    public DrawTree(TreeNode<T> tree) {
        this(tree, null, 0,1);
    }

    public DrawTree(TreeNode<T> tree, DrawTree<T> parent, int depth, int number) {
        this.x = -1;
        this.y = depth;
        this.tree = tree;
        this.childs = IntStream.range(0, tree.getChilds().size())
                        .mapToObj(index->new DrawTree<>(tree.getChilds().get(index), this, depth +1, index +1))
                        .collect(Collectors.toList());
        this.parent = parent;
        this.thread = null;
        this.offset = 0;
        this.ancestor = this;
        this.change = this.shift = this.mod = 0;
        this._lmostSibling = null;
        this.number = number;
    }

    public DrawTree<T> leftBrother(){
        DrawTree<T> result = null;
        if(this.parent !=null){
            for (DrawTree<T> subTree : parent.childs){
                if(subTree == this) {
                    return result;
                } else {
                    result = subTree;
                }
            }
        }
        return result;
    }

    public DrawTree<T> getLmostSibling(){
        if(this._lmostSibling == null && this.parent != null && this != this.parent.childs.get(0)){
            this._lmostSibling =  this.parent.childs.get(0);
        }
        return  this._lmostSibling;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public List<DrawTree<T>> getChilds() {
        return childs;
    }

    public TreeNode<T> getTree() {
        return tree;
    }

    public DrawTree<T> left(){
        if(this.thread != null){
            return this.thread;
        } else if(this.childs.size() > 0){
            return this.childs.get(0);
        }
        return null;
    }

    public DrawTree<T> right(){
        if(this.thread != null){
            return this.thread;
        } else if(this.childs.size() > 0){
            return this.childs.get(this.childs.size() -1);
        }
        return null;
    }


    public static <T> double[] buchheim(DrawTree<T> tree){
        DrawTree<T> dt = firstWalk(tree);
        return secondWalk(dt);
    }

    public static <T> DrawTree<T> firstWalk(DrawTree<T> tree){
        return firstWalk(tree, 1.0);
    }

    public static <T> DrawTree<T> firstWalk(DrawTree<T> tree, double distance){
        if(tree.childs.size() == 0){
            if(tree.getLmostSibling() != null){
                tree.x = tree.leftBrother().x + distance;
            } else {
                tree.x = 0;
            }
        }
        else {
            DrawTree<T> defaultAncestor = tree.childs.get(0);
            for (DrawTree<T> subChild :
                    tree.childs) {
                System.out.println(subChild.getTree().getNode());
                firstWalk(subChild, distance);
                defaultAncestor = apportion(subChild, defaultAncestor, distance);
            }

            executeShifts(tree);
            double midpoint = (tree.childs.get(0).x + tree.childs.get(tree.childs.size() - 1).x) / 2;
            DrawTree<T> treeLeftBroth = tree.leftBrother();

            if(treeLeftBroth != null){
                tree.x = treeLeftBroth.x + distance;
                tree.mod = tree.x - midpoint;
            } else {
                tree.x = midpoint;
            }
        }
        return tree;
    }

    private static <T> DrawTree<T> apportion(DrawTree<T> tree, DrawTree<T> ancestor, double distance){
        DrawTree<T> leftBrother = tree.leftBrother();

        if(leftBrother != null){
            DrawTree<T> vir = tree,vor = tree;
            DrawTree<T> vil = leftBrother;
            DrawTree<T> vol = tree.getLmostSibling();

            double sir = tree.mod, sor = tree.mod;
            double sil = vil.mod;
            double sol = vol.mod;

            while(vil.right() != null && vir.right() !=null){
                vil = vil.right();
                vir = vir.left();
                vol = vol.left();
                vor = vor.right();
                vor.ancestor = tree;
                double shift = (vil.x + sil) - (vir.x + sir) + distance;
                if(shift > 0){
                    DrawTree<T> a = ancestor(vil, tree, ancestor);
                    moveSubtree(a, tree, shift);
                    sir = sir + shift;
                    sor = sor + shift;
                }
                sil += vil.mod;
                sir += vir.mod;
                sol += vol.mod;
                sor += vor.mod;
            }

            if(vil.right() != null && vol.right() == null){
                vor.thread = vil.right();
                vor.mod += sil - sor;
            } else {
                if(vir.left() != null && vol.left() == null){
                    vol.thread = vir.left();
                    vol.mod += sir - sol;
                }
                ancestor = tree;
            }
        }

        return ancestor;
    }
    private static <T> void moveSubtree(DrawTree<T> wl, DrawTree<T> wr, double shift){
        int subtrees = wr.number - wl.number;
        wr.change -= shift / subtrees;
        wr.shift += shift;
        wl.change += shift / subtrees;
        wr.x += shift;
        wr.mod += shift;
    }
    private static <T> DrawTree<T> ancestor(DrawTree<T> vil, DrawTree<T> v, DrawTree<T> defaultAncestor){
        if(v.childs.contains(vil.ancestor)){
            return vil.ancestor;
        } else {
            return defaultAncestor;
        }
    }
    private static <T> void executeShifts(DrawTree<T> tree){
        double shift =0, change = 0;

        for (int i = tree.childs.size() -1; i >=0; i--){
            DrawTree<T> subTree = tree.childs.get(i);
            subTree.x += shift;
            subTree.mod += shift;
            change += subTree.change;
            shift += subTree.shift + change;
        }
    }
    private static <T> double[] secondWalk(DrawTree<T> tree){
        return secondWalk(tree,0,0);
    }
    private static <T> double[] secondWalk(DrawTree<T> tree, double m, int depth){
        tree.x += m;
        tree.y = depth;

        double[] result = new double[2];
        result[0] =  tree.x;
        result[1] =  tree.y;

        for (DrawTree<T> subTree : tree.childs){
            double[] childResult = secondWalk(subTree, m + tree.mod, depth + 1);

            result[0] = Math.max(result[0], childResult[0]);
            result[1] = Math.max(result[1], childResult[1]);
        }

        return result;
    }
}
