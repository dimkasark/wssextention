package sample.draw.clear;

import java.util.List;

/**
 * Created by dimka on 3/20/2017.
 */
public class Tree<T> {
    double w, h; // Width and height.
    double x, y, prelim, mod, shift, change;
    Tree<T> tl, tr; // Left and right thread.
    Tree<T> el, er; // Extreme left and right nodes.
    double msel, mser; // Sum of modifiers at the extreme nodes.

    List<Tree<T>> c;
    private T node;


    public Tree(T node, double w, double h, double y, List<Tree<T>> child) {
        this.node = node;
        this.w = w;
        this.h = h;
        this.y = y;
        this.c = child;
    }

    public List<Tree<T>> getChild() {
        return c;
    }

    public T getNode() {
        return node;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
