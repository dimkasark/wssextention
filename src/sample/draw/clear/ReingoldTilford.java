package sample.draw.clear;

/**
 * Created by dimka on 3/20/2017.
 */
public class ReingoldTilford {

    public <T> double layout(Tree<T> t) {
        firstWalk(t);
        return secondWalk(t, 0);
    }

    private <T> void firstWalk(Tree<T> t) {
        if (t.c.size() == 0) {
            setExtremes(t);
            return;
        }
        firstWalk(t.c.get(0));
        // Create siblings in contour minimal vertical coordinate and index list.
        IYL ih = updateIYL(bottom(t.c.get(0).el), 0, null);
        for (int i = 1; i < t.c.size(); i++) {
            firstWalk(t.c.get(i));
            //Store lowest vertical coordinate while extreme nodes still point in current subtree.
            double minY = bottom(t.c.get(i).er);
            separate(t, i, ih);
            ih = updateIYL(minY, i, ih);
        }
        positionRoot(t);
        setExtremes(t);
    }

    private <T> void setExtremes(Tree<T> t) {
        if (t.c.size() == 0) {
            t.el = t;
            t.er = t;
            t.msel = t.mser = 0;
        } else {
            t.el = t.c.get(0).el;
            t.msel = t.c.get(0).msel;
            t.er = t.c.get(t.c.size() -1).er;
            t.mser = t.c.get(t.c.size() -1).mser;
        }
    }

    private <T> void separate(Tree<T> t, int i, IYL ih) {
        // Right contour node of left siblings and its sum of modfiers.
        Tree<T> sr = t.c.get(i - 1);
        double mssr = sr.mod;
        // Left contour node of current subtree and its sum of modfiers.
        Tree<T> cl = t.c.get(i);
        double mscl = cl.mod;
        while (sr != null && cl != null) {
            if (bottom(sr) > ih.lowY) ih = ih.nxt;
            // How far to the left of the right side of sr is the left side of cl?
            double dist = (mssr + sr.prelim + sr.w) - (mscl + cl.prelim);
            if (dist > 0) {
                mscl += dist;
                moveSubtree(t, i, ih.index, dist);
            }
            double sy = bottom(sr), cy = bottom(cl);
            // Advance highest node(s) and sum(s) of modifiers (Coordinate system increases       downwards)
            if (sy <= cy) {
                sr = nextRightContour(sr);
                if (sr != null) mssr += sr.mod;
            }
            if (sy >= cy) {
                cl = nextLeftContour(cl);
                if (cl != null) mscl += cl.mod;
            }
        }
        // Set threads and update extreme nodes.
        // In the first case, the current subtree must be taller than the left siblings.
        if (sr == null && cl != null) setLeftThread(t, i, cl, mscl);
            // In this case, the left siblings must be taller than the current subtree.
        else if (sr != null && cl == null) setRightThread(t, i, sr, mssr);
    }

    private <T> void moveSubtree(Tree<T> t, int i, int si, double dist) {
        // Move subtree by changing mod.
        t.c.get(i).mod += dist;
        t.c.get(i).msel += dist;
        t.c.get(i).mser += dist;
        distributeExtra(t, i, si, dist);
    }


    private <T> Tree<T> nextLeftContour(Tree<T> t) {
        return t.c.size() == 0 ? t.tl : t.c.get(0);
    }

    private <T> Tree<T>  nextRightContour(Tree<T> t) {
        return t.c.size() == 0 ? t.tr : t.c.get(t.c.size() - 1);
    }

    private <T> double bottom(Tree<T> t) {
        return t.y + t.h;
    }

    private <T> void setLeftThread(Tree<T> t, int i, Tree<T> cl, double modsumcl) {
        Tree li = t.c.get(0).el;
        li.tl = cl;
        // Change mod so that the sum of modifier after following thread is correct.
        double diff = (modsumcl - cl.mod) - t.c.get(0).msel;
        li.mod += diff;
        // Change preliminary x coordinate so that the node does not move.
        li.prelim -= diff;
        // Update extreme node and its sum of modifiers.
        t.c.get(0).el = t.c.get(i).el;
        t.c.get(0).msel = t.c.get(i).msel;
    }

    // Symmetrical to setLeftThread.
    private <T> void setRightThread(Tree<T> t, int i, Tree<T> sr, double modsumsr) {
        Tree ri = t.c.get(i).er;
        ri.tr = sr;
        double diff = (modsumsr - sr.mod) - t.c.get(i).mser;
        ri.mod += diff;
        ri.prelim -= diff;
        t.c.get(i).er = t.c.get(i -1).er;
        t.c.get(i).mser = t.c.get(i -1).mser;
    }

    private <T> void positionRoot(Tree<T> t) {
        // Position root between children, taking into account their mod.
        t.prelim = (t.c.get(0).prelim + t.c.get(0).mod + t.c.get(t.c.size() - 1).mod +
                t.c.get(t.c.size() - 1).prelim + t.c.get(t.c.size() - 1).w) / 2 - t.w / 2;
    }

    private <T> double secondWalk(Tree<T> t, double modsum) {

        modsum += t.mod;
        // Set absolute (non-relative) horizontal coordinate.
        double max = t.x = t.prelim + modsum;
        addChildSpacing(t);
        for (int i = 0; i < t.c.size(); i++){
            double temp = secondWalk(t.c.get(i), modsum);
            if(temp > max){
                max = temp;
            }
        }
        return max;
    }

    private <T> void distributeExtra(Tree<T> t, int i, int si, double dist) {
        // Are there intermediate children?
        if (si != i - 1) {
            double nr = i - si;
            t.c.get(si + 1).shift += dist / nr;
            t.c.get(i).shift -= dist / nr;
            t.c.get(i).change -= dist - dist / nr;
        }
    }

    // Process change and shift to add intermediate spacing to mod.
    private <T> void addChildSpacing(Tree<T> t) {
        double d = 0, modsumdelta = 0;
        for (int i = 0; i < t.c.size(); i++) {
            d += t.c.get(i).shift;
            modsumdelta += d + t.c.get(i).change;

            t.c.get(i).mod += modsumdelta;
        }
    }

    // A linked list of the indexes of left siblings and their lowest vertical coordinate.
    class IYL {
        double lowY;
        int index;
        IYL nxt;

        public IYL(double lowY, int index, IYL nxt) {
            this.lowY = lowY;
            this.index = index;
            this.nxt = nxt;
        }
    }

    IYL updateIYL(double minY, int i, IYL ih) {
        // Remove siblings that are hidden by the new subtree.
        while (ih != null && minY >= ih.lowY) ih = ih.nxt;
        // Prepend the new subtree.
        return new IYL(minY, i, ih);
    }

}
