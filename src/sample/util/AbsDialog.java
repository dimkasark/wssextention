package sample.util;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by dimka on 3/19/2017.
 */
public abstract class AbsDialog extends Stage {
    public AbsDialog(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(getDialogFxml()));
        fxmlLoader.setController(this);

        try {
            setScene(new Scene(fxmlLoader.load()));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected abstract String getDialogFxml();
}
