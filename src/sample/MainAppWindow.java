package sample;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import sample.dialog.route.RouteShowDialog;
import sample.draw.DrawTree;
import sample.models.*;
import sample.util.Action;
import sample.util.ItemView;
import sample.util.SensorUtils;
import sample.util.TreeNode;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MainAppWindow implements Initializable {
    @FXML
    public AnchorPane nodeContainer;

    @FXML
    public Slider zoomSlider;

    /**area controls*/
    @FXML
    public TextField areaMaxX,areaMaxY,areaSensors;

    /**hole controls*/
    @FXML
    public TextField holeMinX,holeMinY,holeMaxX,holeMaxY;

    /**controls*/
    @FXML
    public TextField centralPointId, ringsRadius;
    @FXML
    public TextField centralPointIdRouting, ringsRadiusRouting;

    /**rigs*/
    @FXML
    public Label ringsTextCount;
    @FXML
    public ListView<DeviceRing> ringsListView;

    public Scene scene;

    private Paint[] ringPaints = new Paint[]{
            new Color(1,0,0,1),
            new Color(0,1,0,1),
            new Color(0,0,1,1),
            new Color(1,1,0,1),
            new Color(1,0,1,1),
            new Color(0,1,1,1),
    };



    public static final double ItemSize = 40;

    private List<Device> devices;
    private List<Sensor> sensors;
    private Rings rings;
    private List<DeviceRing> deviceRings;
    private Map<Device, ItemView> deviceView = new HashMap<>();
    private Random random;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        random = new Random(Calendar.getInstance().getTimeInMillis());
        zoomSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            nodeContainer.setScaleX(newValue.doubleValue());
            nodeContainer.setScaleY(newValue.doubleValue());
        });
        ringsListView.setItems(FXCollections.observableArrayList());
        ringsListView.setCellFactory(param -> new Cell(this::drawRing));
    }

    private List<TreeNode<String>> generateRandomGrid(int depth, int depthMax, int maxChildCount, String address){
        List<TreeNode<String>> result = new ArrayList<>();
        if(depth >= depthMax) return result;
        int childCount =  1 + random.nextInt(maxChildCount);
        for(int i= 0; i< childCount; i++){
            String newArddress = address + i;
            result.add(new TreeNode<>(newArddress, generateRandomGrid(depth +1, depthMax, maxChildCount, newArddress)));
        }
        return result;

    }

    public void createNewArea(){
        try {
            nodeContainer.getChildren().clear();

            int maxX = Integer.parseInt(areaMaxX.textProperty().get());
            int maxY = Integer.parseInt(areaMaxY.textProperty().get());
            int sensorsCount = Integer.parseInt(areaSensors.textProperty().get());

            nodeContainer.setPrefSize(ItemSize * maxX, ItemSize * maxY);

            sensors = SensorUtils.generateField(maxX,maxY,sensorsCount);
            devices = SensorUtils.generateDevices(sensors);

            drawArea();

        } catch (Exception ignored){
        }
    }

    public void createHole(){
        if(isAreaCreated()){
            try {
                int minX = Integer.parseInt(holeMinX.textProperty().get());
                int minY = Integer.parseInt(holeMinY.textProperty().get());
                int maxX = Integer.parseInt(holeMaxX.textProperty().get());
                int maxY = Integer.parseInt(holeMaxY.textProperty().get());

                SensorUtils.createHoleInArea(devices, minX, minY, maxX, maxY, (Device device)->{
                    nodeContainer.getChildren().remove(deviceView.get(device));
                    deviceView.remove(device);
                    sensors.removeAll(device.getSensors());
                });
            } catch (Exception ignore){
            }
        }
    }

    public void calculateRings(){
        if(!isAreaCreated()) return;

        try{
            int pointId = Integer.parseInt(centralPointId.textProperty().get());
            double radius = Double.parseDouble(ringsRadius.textProperty().get());

            rings = new RingCreator(sensors, devices).createRingsForCentralPoint(pointId, radius);

            deviceRings =
                    IntStream.range(0, rings.getSize())
                            .mapToObj((index) -> {
                                List<Integer> listSensorId = rings.getRing(index);
                                return new DeviceRing(devices.stream().filter(device ->
                                        device.getSensors()
                                                .stream().filter(sensor -> listSensorId.contains(sensor.getId()))
                                                .findAny().orElse(null) != null).collect(Collectors.toList()), index);
                            })
                            .collect(Collectors.toList());

            /**clear field*/
            devices.forEach((device -> deviceView.get(device).getCircle().setFill(new Color(0,0,0,0))));

            int colorIndex = 0;
            for (DeviceRing ringDevices :
                    deviceRings) {
                ringDevices.setRingPaint(ringPaints[colorIndex]);
                ringDevices.setDraw(true);
                drawRing(ringDevices);

                colorIndex++;
                if(colorIndex >= ringPaints.length){
                    colorIndex = 0;
                }
            }

            ringsListView.getItems().clear();
            ringsListView.getItems().addAll(deviceRings);
            ringsTextCount.setText("" + deviceRings.size());


        } catch (Exception ignore){
        }
    }

    public void toggleAllRings(){
        if(deviceRings != null){
            boolean isDrawAll = !deviceRings.stream()
                    .map(DeviceRing::isDraw)
                    .reduce(true, (left, right)-> left && right);

            deviceRings.forEach((deviceRing -> {
                deviceRing.setDraw(isDrawAll);
                drawRing(deviceRing);
            }));

            forceListRefreshOn(ringsListView);
        }
    }

    public void calculateRoutingTable(){
        if(!isAreaCreated()) return;

        try {
            int pointId = Integer.parseInt(centralPointIdRouting.textProperty().get());
            double radius = Double.parseDouble(ringsRadiusRouting.textProperty().get());

            Device device = devices.stream()
                    .filter(deviceInner -> deviceInner.getSensors().stream().filter(sensor -> sensor.getId() == pointId).findFirst().orElse(null) != null)
                    .findFirst().orElse(null);


            if(device !=null){
                devices.forEach(deviceInner-> deviceInner.setAddress(null));
                device.setAddress("");

                new RouteCreator(devices).calculateRoutingTable(device, radius);

                RouteShowDialog dialog = new RouteShowDialog(devices);
                dialog.show();
            }
        } catch (Exception ignore){
        }
    }

    private void drawRing(DeviceRing ring){
        for (Device device :
                ring.getDevices()) {
            deviceView.get(device).getCircle().setFill(ring.getRingPaint());
        }
    }

    public void exportData(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select save file");
        fileChooser.getExtensionFilters().add( new FileChooser.ExtensionFilter("Sensor Configuration File", "cfg"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
        fileChooser.setInitialFileName("sensor_config_" + sdf.format(Calendar.getInstance().getTime()) + ".cfg");
        File file = fileChooser.showSaveDialog(nodeContainer.getScene().getWindow());
        if(file!=null) {
            FileWriter fileWriter = null;
            try{
                fileWriter = new FileWriter(file);
                fileWriter.write(new Gson().toJson(sensors));
            } catch (Exception ignored){
            }
             finally {
                if(fileWriter != null){
                    try {
                        fileWriter.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    public void importData(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select configuration file");
        fileChooser.getExtensionFilters().add( new FileChooser.ExtensionFilter("Sensor Configuration File", "*.cfg"));

        File file = fileChooser.showOpenDialog(nodeContainer.getScene().getWindow());
        if(file!=null) {
            FileReader reader = null;
            try{
                reader = new FileReader(file);
                Type collectionType = new TypeToken<List<Sensor>>(){}.getType();

                sensors = new Gson().fromJson(reader, collectionType);
                devices = SensorUtils.generateDevices(sensors);
                drawArea();
            } catch (Exception ignored){
            }
            finally {
                if(reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    private boolean isAreaCreated() {
        return devices !=null && !devices.isEmpty();
    }

    private void drawArea() {
        nodeContainer.getChildren().clear();
        deviceView.clear();
        for (Device device : devices
             ) {
            String id = "" + device.getSensors().get(0).getId();
            ItemView item = createViewPoint(device.getSensors().size() > 1 ? id + ",..": id);

            item.getContainer().setLayoutX(device.getX()*ItemSize);
            item.getContainer().setLayoutY(device.getY()* ItemSize);

            nodeContainer.getChildren().add(item.getContainer());
            deviceView.put(device, item);
        }
    }

    private ItemView createViewPoint(String id){
        return createViewPoint(id,ItemSize);
    }

    private ItemView createViewPoint(String id, double size){
        return new ItemView(id, size);
    }

    private <T> void forceListRefreshOn(ListView<T> lsv) {
        ObservableList<T> items = lsv.<T>getItems();
        lsv.<T>setItems(null);
        lsv.<T>setItems(items);
    }

    public class Cell extends ListCell<DeviceRing>{
        private Action<DeviceRing> drawRingCallback;

        @FXML
        protected HBox container;
        @FXML
        protected Rectangle ringColor;
        @FXML
        protected Label ringText;

        public Cell(Action<DeviceRing> drawRingCallback) {
            this.drawRingCallback = drawRingCallback;
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/listItem.fxml"));
            fxmlLoader.setController(this);
            try {
                container = fxmlLoader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            this.setPrefWidth(0);
            container.prefWidthProperty().bind(this.widthProperty());
        }

        @Override
        protected void updateItem(DeviceRing item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                clearContent();
            } else {
                addContent(item);
            }
        }

        public void toggleRingView(){
            DeviceRing ring = getItem();
            ring.setDraw(!ring.isDraw());
            ringColor.setFill(ring.getRingPaint());

            drawRingCallback.action(ring);
        }

        private void clearContent() {
            setText(null);
            setGraphic(null);
        }

        private void addContent(DeviceRing cache) {
            setText(null);
            ringColor.setFill(cache.getRingPaint());
            ringText.setText(String.format("Ring %d", cache.getRingNumber()));
            setGraphic(container);
        }
    }
}
