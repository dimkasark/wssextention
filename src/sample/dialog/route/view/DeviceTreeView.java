package sample.dialog.route.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;

/**
 * Created by dimka on 3/20/2017.
 */
public class DeviceTreeView {
    private StackPane container;
    private Circle circle;
    private Text text;
    private Text leftText;
    private Text rightText;

    public DeviceTreeView(double size, double padding) {
        this.container = new StackPane();
        this.container.setMaxSize(size,size);
        this.container.setMinSize(size,size);

        this.circle = new Circle(size/2.0 - padding);
        this.circle.setStroke(new Color(0,0,0,1));
        this.circle.setStrokeWidth(1);
        this.circle.setFill(new Color(0,0,0,0));

        this.text = new Text();
        this.text.setBoundsType(TextBoundsType.VISUAL);
        this.text.setFont(Font.font("Arial", 15));

        this.leftText = new Text();
        this.leftText.setBoundsType(TextBoundsType.VISUAL);
        this.leftText.setFont(Font.font("Arial", 15));
        StackPane.setAlignment(this.leftText, Pos.CENTER_RIGHT);
        StackPane.setMargin(this.leftText, new Insets(0,size,0,0));

        this.rightText = new Text();
        this.rightText.setBoundsType(TextBoundsType.VISUAL);
        this.rightText.setFont(Font.font("Arial", 15));
        StackPane.setAlignment(this.rightText, Pos.CENTER_LEFT);
        StackPane.setMargin(this.rightText, new Insets(0,0,0,size));

        this.container.getChildren().addAll(this.circle, this.text, this.leftText, this.rightText);
    }

    public StackPane getContainer() {
        return container;
    }

    public Circle getCircle() {
        return circle;
    }

    public Text getText() {
        return text;
    }

    public Text getLeftText() {
        return leftText;
    }

    public Text getRightText() {
        return rightText;
    }
}
